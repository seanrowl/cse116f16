package code;

/**
 * This class will contain useful methods and fields for each Player on the board. Gameplay functionality relating
 * to Player's will appear here.
 * @author Jared Brown, Sean Rowland
 */

import java.util.LinkedList;
import java.util.Scanner;

public class Player {

	/**Name of the token's character*/
	private String name;
	
	/**Player's x coordinate on the board*/
	private int posx;
	
	/**Player's y coordinate on the board*/
	private int posy;
	
	/**Cards dealt to the Player at the beginning of the game*/
	private LinkedList<String> cards;
	
	/**Cards the Player has seen or have been proved false in a suggestion*/
	private LinkedList<String> notebook;
	
	/**Current tile the Player is standing on, on the board*/
	private Object currentSpace;
	
	/**Tells whether the Player is actually playing or is not*/
	private boolean activated;
	
	/**Tells whether the player can make a suggestion*/
	private boolean suggestion;
		
	/**Tell's whether the Player made an Assumption incorrectly*/
	private boolean lost;
	
	/**Tells whether the player has been moved by a suggestion of another player
	 * since their last turn
	 */
	public boolean moveBySuggestion;
	
	/**Constructor for the Player object. Sets the name, starting position and current space
	 *  of the player to {@character}, {@startingx}, {@startingy} and {@startSpace} respectively.
	 * 
	 * @param character - String that contains the name of the character in the game
	 * @param startingx - int of the players starting x position on the board
	 * @param startingy - int of the players starting y position on the board
	 * @param startSpace - Object of the square the player starts on
	 */
	public Player(String character, int startingx, int startingy, Object startSpace){
		name = character;
		posx = startingx;
		posy = startingy;
		currentSpace = startSpace;
		cards = new LinkedList<String>();
		notebook = new LinkedList<String>();
		Board.set(startingx,startingy,new Player());
	}
	
	/**
	 * Blank constructor for Player class, used for instantiations of the Player class
	 * for comparisons in other methods
	 */
	public Player(){
		
	}
	
	/**
	 * Adds the card passed to the cards LinkedList held by the player object
	 * @param card - card being added to the cards LinkedList
	 */
	public void addCard(String card){
		cards.add(card);
		notebook.add(card);
	}
	
	/**
	 * Method that returns the cards held by the player
	 * @return LinkedList<String> cards held by the player
	 */
	public LinkedList<String> getCards(){
		return cards;
	}
	
	/**
	 * Method that returns the cards in the player's notebook
	 * @return LinkedList<String> cards in the player's notebook
	 */
	public LinkedList<String> getNotebook(){
		return notebook;
	}
	
	/**
	 * Method that returns the name of the player
	 * @return String name of the player
	 */
	public String getName(){
		return name;
	}
	
	/**
	 * Method that returns the x and y position of the player on the game board
	 * @return int[] of positions [x,y]
	 */
	public int[] getPos(){
		return new int[]{posx,posy};
	}
	
	/**
	 * Called after a player makes a suggestion to determine whether another player will 
	 * prove the suggestion true or false. Returns the player who proved the suggestion false
	 * and the card(s) the used to prove it false with. If no one proves the suggestion false
	 * an empty LinkedList is returned.
	 * @param suggestionMade - String[] of cards in suggestion
	 * @param players - Player[] of players to the left of player who made the suggestion,
	 * 			in order
	 * @return the player who proved the suggestion false and the card(s) they did it with
	 * 			in a LinkedList<String> with players first, then cards after.
	 */
	public LinkedList<String> suggestionProof(String[] suggestionMade, Object[] players){
		LinkedList<String> retVal = new LinkedList<String>();
		for(Object p: players){
			for(String s: suggestionMade){
				for(String c: ((Player)p).getCards()){
					if(c.toLowerCase().equals(s.toLowerCase())){
						retVal.add(c);
					}
				}
			}
			if (!retVal.isEmpty()){
				retVal.add(0, ((Player)p).getName());
				return retVal;
			}
		}
		return retVal;
	}
	
	/**
	 * Method that takes in {@userInput} and moves the player, return true if they can, false if not.
	 * @param userInput input from the player
	 * @return boolean - true if the player moves, false if not
	 */
	public boolean move(String userInput){
		int legality;
		try{
			switch(userInput){
				case "up":
					legality = moveLegality(posx, posy - 1);
					switch(legality){
					case 0: //player makes an illegal move
						return false;
					case 1: //when player moves and counts against their diceRoll
						Board.set(posx, posy, currentSpace);
						posy -= 1;
						currentSpace = Board.get(posx, posy);
						Board.set(posx, posy, new Player());
						cluedoMain.diceRoll -= 1;
						return true;
					case 2:	//when player moves and it doesn't count against their cluedoMain.diceRoll, ex. within a room
						Board.set(posx, posy, currentSpace);
						posy -= 1;
						currentSpace = Board.get(posx, posy);
						Board.set(posx, posy, new Player());
						return true;
					case 3: //when a player moves to a SecretPassage
						useSecretPassage((SecretPassage)Board.get(posx, posy - 1));
						cluedoMain.diceRoll = 0;
						return true;
					case 4: //when a player moves to a Door
						Board.set(posx, posy, currentSpace);
						posy -= 1;
						currentSpace = Board.get(posx, posy);
						enterRoom((Door)Board.get(posx, posy));
						cluedoMain.diceRoll = 0;
						return true;
					}
					break;
				case "down":
					legality = moveLegality(posx, posy  + 1);
					switch(legality){
					case 0:
						return false;
					case 1:
						Board.set(posx, posy, currentSpace);
						posy += 1;
						currentSpace = Board.get(posx, posy);
						Board.set(posx, posy, new Player());
						cluedoMain.diceRoll -= 1;
						return true;
					case 2:	
						Board.set(posx, posy, currentSpace);
						posy += 1;
						currentSpace = Board.get(posx, posy);
						Board.set(posx, posy, new Player());
						return true;
					case 3: 
						useSecretPassage((SecretPassage)Board.get(posx, posy + 1));
						cluedoMain.diceRoll = 0;
						return true;
					case 4:
						Board.set(posx, posy, currentSpace);
						posy += 1;
						currentSpace = Board.get(posx, posy);
						enterRoom((Door)Board.get(posx, posy));
						cluedoMain.diceRoll = 0;
						return true;
					}
					break;
				case "left":
					legality = moveLegality(posx - 1, posy);
					switch(legality){
					case 0:
						return false;
					case 1:
						Board.set(posx, posy, currentSpace);
						posx -= 1;
						currentSpace = Board.get(posx, posy);
						Board.set(posx, posy, new Player());
						cluedoMain.diceRoll -= 1;
						return true;
					case 2:	
						Board.set(posx, posy, currentSpace);
						posx -= 1;
						currentSpace = Board.get(posx, posy);
						Board.set(posx, posy, new Player());
						return true;
					case 3: 
						useSecretPassage((SecretPassage)Board.get(posx - 1, posy));
						cluedoMain.diceRoll = 0;
						return true;
					case 4:
						Board.set(posx, posy, currentSpace);
						posx -= 1;
						currentSpace = Board.get(posx, posy);
						enterRoom((Door)Board.get(posx, posy));
						cluedoMain.diceRoll = 0;
						return true;	
					}
					break;
				case "right":
					legality = moveLegality(posx + 1, posy);
					switch(legality){
					case 0:
						return false;
					case 1:
						Board.set(posx, posy, currentSpace);
						posx += 1;
						currentSpace = Board.get(posx, posy);
						Board.set(posx, posy, new Player());
						cluedoMain.diceRoll -= 1;
						return true;
					case 2:	
						Board.set(posx, posy, currentSpace);
						posx += 1;
						currentSpace = Board.get(posx, posy);
						Board.set(posx, posy, new Player());
						return true;
					case 3: 
						useSecretPassage((SecretPassage)Board.get(posx + 1, posy));
						cluedoMain.diceRoll = 0;
						return true;
					case 4:
						Board.set(posx, posy, currentSpace);
						posx += 1;
						currentSpace = Board.get(posx, posy);
						enterRoom((Door)Board.get(posx , posy));
						cluedoMain.diceRoll = 0;
						return true;
					}
					break;
			}
		}catch(IndexOutOfBoundsException e){
			return false;
		}
		return false;
	}
	
	
	/**
	 * @deprecated only kept to satisfy original moveLegalityTests*/
	public void move(int diceRoll, String userInput){
		Scanner sc = new Scanner(userInput);
		int legality;
		while(diceRoll > 0){
			String input = sc.next();
			switch(input){
				case "up":
					legality = moveLegality(posx, posy - 1);
					switch(legality){
					case 0: //player makes an illegal move
						break;
					case 1: //when player moves and counts against their diceRoll
						Board.set(posx, posy, currentSpace);
						posy -= 1;
						currentSpace = Board.get(posx, posy);
						Board.set(posx, posy, new Player());
						diceRoll -= 1;
						break;
					case 2:	//when player moves and it doesn't count against their diceRoll, ex. within a room
						Board.set(posx, posy, currentSpace);
						posy -= 1;
						currentSpace = Board.get(posx, posy);
						Board.set(posx, posy, new Player());
						break;
					case 3: //when a player moves to a SecretPassage
						useSecretPassage((SecretPassage)Board.get(posx, posy - 1));
						diceRoll = 0;
						break;
					case 4: //when a player moves to a Door
						Board.set(posx, posy, currentSpace);
						posy -= 1;
						currentSpace = Board.get(posx, posy);
						enterRoom((Door)Board.get(posx, posy));
						diceRoll = 0;
						break;
					}
					break;
				case "down":
					legality = moveLegality(posx, posy  + 1);
					switch(legality){
					case 0:
						break;
					case 1:
						Board.set(posx, posy, currentSpace);
						posy += 1;
						currentSpace = Board.get(posx, posy);
						Board.set(posx, posy, new Player());
						diceRoll -= 1;
						break;
					case 2:	
						Board.set(posx, posy, currentSpace);
						posy += 1;
						currentSpace = Board.get(posx, posy);
						Board.set(posx, posy, new Player());
						break;
					case 3: 
						useSecretPassage((SecretPassage)Board.get(posx, posy + 1));
						diceRoll = 0;
						break;
					case 4:
						Board.set(posx, posy, currentSpace);
						posy += 1;
						currentSpace = Board.get(posx, posy);
						enterRoom((Door)Board.get(posx, posy));
						diceRoll = 0;
						break;
					}
					break;
				case "left":
					legality = moveLegality(posx - 1, posy);
					switch(legality){
					case 0:
						break;
					case 1:
						Board.set(posx, posy, currentSpace);
						posx -= 1;
						currentSpace = Board.get(posx, posy);
						Board.set(posx, posy, new Player());
						diceRoll -= 1;
						break;
					case 2:	
						Board.set(posx, posy, currentSpace);
						posx -= 1;
						currentSpace = Board.get(posx, posy);
						Board.set(posx, posy, new Player());
						break;
					case 3: 
						useSecretPassage((SecretPassage)Board.get(posx - 1, posy));
						diceRoll = 0;
						break;
					case 4:
						Board.set(posx, posy, currentSpace);
						posx -= 1;
						currentSpace = Board.get(posx, posy);
						enterRoom((Door)Board.get(posx, posy));
						diceRoll = 0;
						break;	
					}
					break;
				case "right":
					legality = moveLegality(posx + 1, posy);
					switch(legality){
					case 0:
						break;
					case 1:
						Board.set(posx, posy, currentSpace);
						posx += 1;
						currentSpace = Board.get(posx, posy);
						Board.set(posx, posy, new Player());
						diceRoll -= 1;
						break;
					case 2:	
						Board.set(posx, posy, currentSpace);
						posx += 1;
						currentSpace = Board.get(posx, posy);
						Board.set(posx, posy, new Player());
						break;
					case 3: 
						useSecretPassage((SecretPassage)Board.get(posx + 1, posy));
						diceRoll = 0;
						break;
					case 4:
						Board.set(posx, posy, currentSpace);
						posx += 1;
						currentSpace = Board.get(posx, posy);
						enterRoom((Door)Board.get(posx , posy));
						diceRoll = 0;
						break;
					}
					break;
			}
		}
		sc.close();
	}
	
	/**
	 * Helper method for the move method, tests whether the move input by the player is a 
	 * legal one 
	 * @param newPosx - posx of the attempted move location
	 * @param newPosY - posy of the attempted move location
	 * @return 0 if move illegal, 1 if move legal and decrement dice roll, 
	 * 		2 if move legal but doesn't decrement dice roll, 
	 * 		3 if move is to secret passage, 4 if move is to a door from a hallway
	 */
	public int moveLegality(int newPosx, int newPosY){ //called from move method
		Object spaceToBeMovedTo = Board.get(newPosx, newPosY);
		if(currentSpace.equals(new Hallway())){
			if(spaceToBeMovedTo == null){
				return 0;
			}else if(spaceToBeMovedTo.equals(new Hallway())){
				return 1;
			}else if(spaceToBeMovedTo.equals(new Room())){
				if(moveBySuggestion){
					return 2;
				}else{
					return 0;
				}
			}else if(spaceToBeMovedTo.equals(new Door())){
				return 4;
			}else if(spaceToBeMovedTo.equals(new Player())){
				return 0;
			}else if(spaceToBeMovedTo.equals(new SecretPassage())){
				return 0;
			}
		}else if(currentSpace.equals(new Room())){
			if(spaceToBeMovedTo == null){
				return 0;
			}else if(spaceToBeMovedTo.equals(new Hallway())){
				return 0;
			}else if(spaceToBeMovedTo.equals(new Room())){
				return 2;
			}else if(spaceToBeMovedTo.equals(new SecretPassage())){
				return 3;
			}else if(spaceToBeMovedTo.equals(new Door())){
				return 2;
			}else if(spaceToBeMovedTo.equals(new Player())){
				return 0;
			} 
		}else if(currentSpace.equals(new Door())){
			if(spaceToBeMovedTo == null){
				return 0;
			}else if(spaceToBeMovedTo.equals(new Hallway())){
				return 1;
			}else if(spaceToBeMovedTo.equals(new Room())){
				return 2;
			}else if(spaceToBeMovedTo.equals(new Door())){
				return 2;
			}else if(spaceToBeMovedTo.equals(new Player())){
				return 0;
			}else if(spaceToBeMovedTo.equals(new SecretPassage())){
				return 0;
			}
		}
		return 0;
	}
	
	/**
	 * Helper method for the move method, moves the Player into the correct room based the
	 * location of {@sp}
	 * @param sp - SecretPasage object, used to find location of the secret passage
	 */
	public void useSecretPassage(SecretPassage sp){
		int i = sp.getLocation();//write getLocation for SecretPassage
		switch(i){
		case 0: //top left secret passage
			moveToNewRoom(20,20);
			break;
		case 1: //top right secret passage
			moveToNewRoom(1,21);
			break;
		case 2: //bottom left secret passage
			moveToNewRoom(18,2);
			break;
		case 3: //bottom right secret passage
			moveToNewRoom(1,1);
			break;
		}
	}
	
	/**
	 * Helper method for the move method, moves the Player into the correct room based the
	 * location of {@door}
	 * @param door - Door object, used to find location of the door
	 */
	public void enterRoom(Door door){
		int i = door.getLocation();
		switch(i){
		case 0: //study
			moveToNewRoom(1,1);
			break;
		case 1: //hall
			moveToNewRoom(10,1);
			break;
		case 2: //lounge
			moveToNewRoom(18,2);
			break;
		case 3: //library
			moveToNewRoom(1,7);
			break;
		case 4: //dining room
			moveToNewRoom(19,10);
			break;
		case 5: //billiard room
			moveToNewRoom(1,13);
			break;
		case 6: //conservatory
			moveToNewRoom(1,21);
			break;
		case 7: //ballroom
			moveToNewRoom(10,20);
			break;
		case 8: //kitchen
			moveToNewRoom(20,20);
			break;
		}
	}
	
	/**Helper method for enterRoom and useSecretPassage methods. Moves the player into 
	 * a new room based on {@x} {@y} position passed in. Loops through tiles in a room
	 * until the player can be placed within it
	 * 
	 * @param x - x coordinate of first tile to attempt moving to
	 * @param y - y coordinate of first tile to attempt moving to
	 */
	public void moveToNewRoom(int x, int y){
		suggestion = true;
		int[] currentPosition = new int[]{posx,posy};
		int newx = x;
		int newy = y;
		while((posx == currentPosition[0]) && (posy == currentPosition[1])){
			if(moveLegality(newx,newy) == 2){
				Board.set(posx, posy, currentSpace);
				currentSpace = Board.get(newx, newy);
				Board.set(newx, newy, new Player());
				posx = newx;
				posy = newy;
			}else{
				if(newx < x+2){
					newx ++;
				}else{
					newx = x;
					newy++;
				}
			}
		}
	}
	
	/**Sets the player's activated state to true*/
	public void setActive(){
		activated = true;
	}
	
	/**
	 * Returns whether the player can make a suggestion or not
	 * @return true if player can make a suggestion, false if not
	 */
	public boolean canMakeASuggestion(){
		return suggestion;
	}
	
	/**
	 * Returns whether the player is an active player or not
	 * @return true if player activated, false if not
	 */
	public boolean isActivated(){
		return activated;
	}
	
	/**
	 * Sets whether the player can make a suggestion or not
	 * @param b - boolean suggestion is to be set to
	 */
	public void setSuggestion(boolean b){
		suggestion = b;
	}
	
	/**
	 * Returns the current space the player is standing on top of on the board
	 * @return the current space held by the player
	 */
	public Object getCurrentSpace(){
		return currentSpace;
	}
	
	/**
	 * Adds a card to the player's notebook
	 * @param s - the card to be added to the notebook
	 */
	public void addToNotebook(String s){
		if(!notebook.contains(s)){
			notebook.add(s);
		}
	}
	
	/**
	 * Sets the Player's lost variable to true.
	 */
	public void lost(){
		lost = true;
	}
	
	/**
	 * Returns whether the player has lost the game or not
	 * @return true if player has lost, false if not
	 */
	public boolean isLoser(){
		return lost;
	}
	
	/**
	 * Overridden equals method for the Player class. Returns if {@obj} is an instance of Player.
	 * @param obj - object to be compared
	 * @return true if {@obj} is an instance of a Player, false if not
	 */
	public boolean equals(Object obj){
		return (obj instanceof Player);
	}
}
