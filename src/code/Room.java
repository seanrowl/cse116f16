package code;

/**
 * This class was made to represent a room tile on the game board. Contains a method to compare whether 
 * or not another tile is a Room.
 * @author Sean Rowland, Jared Brown
 *
 */
public class Room {

	/**Name of the room, for suggestion purposes*/
	private String name;
	
	/**
	 * Constructor for Rooms on the board. Sets {@name}, the name of the room on the
	 * board to s
	 * @param s - the name of the room on the board
	 */
	public Room(String s){
		name = s;
	}
	
	/**
	 * Constructor used for comparisons
	 */
	public Room(){
		
	}
	
	/**
	 * Returns the name of the room
	 * @return String, the name of the room
	 */
	public String getName(){
		return name;
	}
	
	/**
	 * Overridden equals method to compare whether {@obj} is a Room object or not.
	 * @param obj - object to be compared
	 * @return true if it is, false if not
	 */
	public boolean equals(Object obj){
		return (obj instanceof Room);
	}
}
