package code;

/**
 * Class that will act as a door when placed on the game board. Contains methods to compare to another
 *  object and to get the door's location, as well as constructors. 
 * @author Sean Rowland, Jared Brown
 *
 */

public class Door {
	
	/**Number that associates the Door object with the room it is in*/
	int doorNumber;
	
	/**
	 * Constructor for doors on the board. Sets {@doorLocation}, the number that associates each door 
	 * with which room it is in.
	 * @param doorLocation - unique int associating each door with a room
	 */
	public Door(int doorLocation){
		doorNumber = doorLocation;
	}
	
	/**
	 * Constructor used for comparisons
	 */
	public Door(){
		
	}
	
	/**
	 * Method to retrieve a door's location
	 * @return int doorNumber,  unique number for the room the door is in
	 */
	public int getLocation(){
		return doorNumber;
	}
	
	/**
	 * Overridden equals method for the door class. Returns if {@obj} is an instance of Door.
	 * @param obj - object to be compared
	 * @return true if {@obj} is an instance of a Door, false if not
	 */
	public boolean equals(Object obj){
		return (obj instanceof Door);
	}
}