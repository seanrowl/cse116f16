package code;

/**
 * This class was made to represent a secret passage on the game board. Contains a method to compare whether 
 * or not another tile is a SecretPassage.
 * @author Sean Rowland, Jared Brown
 *
 */

public class SecretPassage {
	
	/**Number that associates the SecretPassage object with the room it is in*/
	int secretPassageNumber;
	
	/**
	 * Constructor for SecretPassages on the board. Sets {@secretPassageLocation}, the number that associates each secret passage 
	 * with which room it is in.
	 * @param secretPassageLocation - unique int associating each secret passage with a room
	 */
	public SecretPassage(int secretPassageLocation){
		secretPassageNumber = secretPassageLocation;
	}
	
	/**
	 * Constructor used for comparisons
	 */
	public SecretPassage(){
		
	}
	
	/**
	 * Method to retrieve a secret passage's location
	 * @return int secretPassageNumber, unique number for the room the secret passage is in
	 */
	public int getLocation(){
		return secretPassageNumber;
	}
	
	/**
	 * Overridden equals method, used to compare whether {@obj} is a SecretPassage object or not.
	 * @param obj - object to be compared
	 * @return true if it is, false if not
	 */
	public boolean equals(Object obj){
		return (obj instanceof SecretPassage);
	}
}
