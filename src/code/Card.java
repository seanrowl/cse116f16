package code;

import java.util.Collections;
import java.util.LinkedList;

/**
 * This class was created to hold a deck of cards. Contains methods to check whether 
 * a certain card is held within the deck.
 * @author Sean Rowland, Jared Brown
 *
 */

public class Card{
	
	/**The LinkedList where the cards will be stored, representing a deck of cards*/
	public LinkedList<String> cards;
	
	/**
	 * Constructor method for Card class, sets the cards equal to {@s}. 
	 * Simulates a deck of cards
	 * @param s - LinkedList<String> containing the list of cards to be held in cards
	 */
	public Card(LinkedList<String> s){
		cards = s;
	}
	
	/**
	 * Constructor method for Card class, copies the cards in the String[] {@s} passed
	 * in into {@cards}
	 * @param s - the String[] array to be held by the deck of cards
	 */
	public Card(String[] s){
		cards = new LinkedList<String>();
		for(String c: s){
			cards.add(c);
		}
	}
	
	/**
	 * Empty Card constructer, sets {@cards} equal to an empty LinkedList<String>
	 */
	public Card(){
		cards = new LinkedList<String>();
	}
	
	/**
	 * Shuffles the cards held within {@cards}
	 */
	public void shuffle(){
		for(int i = 0; i < 6; i ++){
			Collections.shuffle(cards);
		}
	}
	
	/**
	 * Removes a card from the deck and returns it
	 * @return the card removed from the deck
	 */
	public String remove(){
		return cards.remove();
	}
	
	/**
	 * Adds a card to the deck of cards
	 * @param c - the card to be added to the deck
	 */
	public void add(Card c){
		for(String x: c.cards){
			cards.add(x);
		}
	}
	
	/**
	 * Adds a String[] containing multiple cards that are to be added to the deck
	 * @param c - String[] containing the cards that are to be added to the deck
	 */
	public void add(String[] c){
		for(String x: c){
			cards.add(x);
		}
	}
	
	/**
	 * Returns whether the current deck of cards is empty
	 * @return true if deck is empty, false if not
	 */
	public boolean isEmpty(){
		return cards.isEmpty();
	}
	
	/**
	 * Checks if {@card} is contained by the Card class it is called on
	 * @param card - String to be checked
	 * @return true if the string passed in in the cards array, otherwise false
	 */
	public boolean contains(String card){
		for(String x: cards){
			if(x.toLowerCase().equals(card.toLowerCase())){
				return true;
			}
		}
		return false;
	}
}
