package code;

/**
 * This class was made to represent a hallway tile on the game board. Contains a method to compare whether 
 * or not another tile is a Hallway.
 * @author Sean Rowland, Jared Brown
 *
 */

public class Hallway {
	
	/**
	 * Overridden equals method to compare whether {@obj} is a Hallway object or not.
	 * @param obj - object to be compared
	 * @return true if it is, false if not
	 */
	public boolean equals(Object obj){
		return (obj instanceof Hallway);
	}
}
