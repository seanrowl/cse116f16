package code;

/**Class that runs the entire Cluedo game and contains the event driven methods
 * to run the game. Game must be ran from this method.
 * 
 * ***************************
 * SCALED FOR A 1080P SCREEN *
 * ***************************
 * 
 * @author Jared Brown
 */

import java.util.LinkedList;
import java.util.Queue;

public class cluedoMain {
	
	/**The Player whose turn it is*/
	static Player currentPlayer;
	
	/**The number of people playing*/
	static int numberOfPlayers;
	
	/**The UI interface of the game*/
	static Gui UI;
	
	/**The underlying board data structure for the game*/
	static Board board;
	
	/**All Players in order of turns*/
	static Queue<Player> playerQueue = new LinkedList<Player>();
	
	/**All Room Cards, used for dealing out initial cards*/
	static Card roomCards = new Card(new String[]{
			"Hall","Ballroom","Conservatory","Kitchen","Billiard Room","Study","Dining Room","Library","Lounge"});
	
	/**All Player Cards, used for dealing out initial cards*/
	static Card playerCards = new Card(new String[]{
			"Mrs. Scarlett","Col. Mustard", "Mrs. White","Mr. Green","Mrs. Peacock","Prof. Plum"});
	
	/**All Weapon Cards, used for dealing out initial cards*/
	static Card weaponCards = new Card(new String[]{
			"Candle Stick","Knife","Revolver","Rope","Lead Pipe","Wrench"});
	
	/**All Room Cards, used for comparisons*/
	final static Card allRoomCards = new Card(new String[]{
			"Hall","Ballroom","Conservatory","Kitchen","Billiard Room","Study","Dining Room","Library","Lounge"});
	
	/**All Player Cards, used for comparisons*/
	final static Card allPlayerCards = new Card(new String[]{
			"Mrs. Scarlett","Col. Mustard", "Mrs. White","Mr. Green","Mrs. Peacock","Prof. Plum"});
	
	/**All Weapon Cards, used for comparisons*/
	final static Card allWeaponCards = new Card(new String[]{
			"Candle Stick","Knife","Revolver","Rope","Lead Pipe","Wrench"});
	
	/**Cards held in the Cluedo Envelope*/
	static String[] cardsInEnvelope = new String[3];
	
	/**Current Player's remaining dice roll*/
	static int diceRoll;
	
	/**Method ran when game started, instantiates the Gui and the board*/
	static public void main(String[] args){
		UI = new Gui();
		board = new Board();
	}
	
	/**
	 * Called by Gui when players click how many players there are and sets up 
	 * the game for that number of players
	 * @param i - integer of how many players there are
	 */
	static public void run(int i){
		numberOfPlayers = i;
		Player p1 = new Player("Mrs. Scarlett",16,0, new Hallway());
		Player p2 = new Player("Prof. Plum",0,5, new Hallway());
		Player p3 = new Player("Mr. Green",9,24, new Hallway());
		Player p4 = new Player("Mrs. White",14,24, new Hallway());
		Player p5 = new Player("Mrs. Peacock",0,18, new Hallway());
		Player p6 = new Player("Col. Mustard",23,7, new Hallway());
		playerQueue.add(p1);
		playerQueue.add(p2);
		playerQueue.add(p3);
		playerQueue.add(p4);
		playerQueue.add(p5);
		playerQueue.add(p6);
		roomCards.shuffle();
		playerCards.shuffle();
		weaponCards.shuffle();
		cardsInEnvelope[0] = roomCards.remove();
		cardsInEnvelope[1] = playerCards.remove();
		cardsInEnvelope[2] = weaponCards.remove();
		for(String s: cardsInEnvelope){
			System.out.println(s);
		}
		Card allCards = new Card();
		allCards.add(roomCards);
		allCards.add(playerCards);
		allCards.add(weaponCards);
		allCards.shuffle();
		for(int n = 0; n < 6; n ++){
			Player tmp = playerQueue.remove();
			if(n < i){
				tmp.setActive();
			}
			playerQueue.add(tmp);
		}
		Object[] tmp = playerQueue.toArray();
		int t = 0;
		while (!allCards.isEmpty()){
			if(((Player)tmp[t]).isActivated()){
				((Player)tmp[t]).addCard(allCards.remove());
			}
			t = (t+1) % 6; //cycles through 0-5
		}
		nextPlayer();
	}
	
	/**
	 * Method called at the end of a player's turn, selects the next player
	 * and sets the GUI up for said player
	 */
	static public void nextPlayer(){
		Player tmp = getNextPlayer();
		currentPlayer = tmp;
//		currentPlayer = playerQueue.remove();
//		playerQueue.add(currentPlayer);
		if(currentPlayer == null){
			UI.endGame();
		}else{
			UI.left.setEnabled(true);
			UI.right.setEnabled(true);
			UI.up.setEnabled(true);
			UI.down.setEnabled(true);
			if(currentPlayer.isActivated()){
				if(currentPlayer.canMakeASuggestion()){
					UI.openSuggestionText("start");
				}else{
					UI.makeSuggestion.setEnabled(false);
				}
				UI.setName(currentPlayer);
				UI.setCards(currentPlayer);
				UI.setNotebook(currentPlayer);
				if(!currentPlayer.canMakeASuggestion()){
					UI.rollDie();
				}
				UI.x = currentPlayer.getPos()[0];
				UI.y = currentPlayer.getPos()[1];
			}else{
				nextPlayer();
			}
		}
	} 
	/**
	 * Gets the next player in the playerQueue, checks to see if player has not lost
	 * and is an actual player (activated). Returns the Player to go next or null if
	 * no one can play.
	 * @return the next Player or null if no one can play
	 */
	private static Player getNextPlayer(){
		Player retVal = playerQueue.remove();
		playerQueue.add(retVal);
		while(retVal != currentPlayer){
			if(!retVal.isLoser() && retVal.isActivated()){
				return retVal;
			}
			retVal = playerQueue.remove();
			playerQueue.add(retVal);
		}
		if(!currentPlayer.isLoser()){
			return currentPlayer;
		}
		return null;
	}
}
