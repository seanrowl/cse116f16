package code;

/**Class containing methods and fields of the Cluedo Gui. Instantiates the Gui and
 * contains an action listener for the buttons in the Gui.
 * @author Jared Brown
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.LinkedList;
import java.util.Random;

public class Gui implements ActionListener{
	
	/**x position of the current player's token*/
	int x;
	
	/**y position of the current player's token*/
	int y;
	
	/**JPanel that contains the elements for the board and tokens*/
	JPanel board = new JPanel();
	
	/**JPanel that contains the image of the board*/
	JPanel bg = new JPanel(null);
	
	/**JLabel that is the board image*/
	JLabel img = new JLabel(new ImageIcon("images/clueBoardEdit.jpg"));
	
	/**JPanel of Mrs. Scarlett's Token*/
	JPanel p1 = new JPanel();
	
	/**JPanel of Prof. Plum's Token*/
	JPanel p2 = new JPanel();
	
	/**JPanel of Mr. Green's Token*/
	JPanel p3 = new JPanel();
	
	/**JPanel of Mrs. White's Token*/
	JPanel p4 = new JPanel();
	
	/**JPanel of Mrs. Peacock's Token*/
	JPanel p5 = new JPanel();
	
	/**JPanel of Col. Mustard's Token*/
	JPanel p6 = new JPanel();
	
	/**JPanel containing all of the elements for the movement, end turn, suggestion and
	 * accusation buttons
	 */
	JPanel btns = new JPanel();
	
	/**Left movement button*/
	JButton left = new JButton("left");
	
	/**Right movement button*/
	JButton right = new JButton("right");
	
	/**Up movement button*/
	JButton up = new JButton("up");
	
	/**Down movement button*/
	JButton down = new JButton("down");
	
	/**Make a suggestion button*/
	JButton makeSuggestion = new JButton("Make A Suggestion");
	
	/**Make an accusation button*/
	JButton makeAccusation = new JButton("Make An Accusation");
	
	/**End your turn button*/
	JButton endTurn = new JButton("End Turn");
	
	/**JPanel containing the suggestion, accusation and end turn buttons*/
	JPanel suggestAccuseBtns = new JPanel(new BorderLayout());
	
	/**JPanel contating the up and down buttons*/
	JPanel upDownBtns = new JPanel(new BorderLayout());
	
	/**JPanel containing all of the text elements on the side of the screen*/
	JPanel text = new JPanel();
	
	/**JLabel displaying name of the current player*/
	JLabel name = new JLabel("Player: ");
	
	/**JLabel displaying the dice roll left*/
	JLabel diceRoll = new JLabel("Moves Left: ");
	
	/**JLabel displaying the current player's cards*/
	JLabel cards = new JLabel("Cards: ");
	
	/**JLabel displaying the current player's notebook*/
	JLabel notebook = new JLabel("Notebook: ");
	
	/**Shows/hides the current player's cards*/
	JButton cardToggle = new JButton("Show/Hide Cards");
	
	/**Shows/hides the current player's notebook*/
	JButton notebookToggle = new JButton("Show/Hide Notebook");
	
	/**Main JFrame containing all elements and element containers*/
	JFrame frame = new JFrame("Who Killed Mr. Body-do");
	
	/**JFrame displayed when the player clicks the make a suggestion button
	 * or make an accusation button, contains text fields for user input and
	 * buttons to cancel or confirm suggestion/accusation
	 */
	JFrame suggestionFrame = new JFrame();
	
	/**JPanel containing the text fields for user input on a suggestion/accusation*/
	JPanel suggestionFrameInputs = new JPanel(new FlowLayout());
	
	/**Text Field for user input of room in an suggestion/accusation*/
	JTextField suggestionFrameRoom =  new JTextField("room");
	
	/**Text Field for user input of player in an suggestion/accusation*/
	JTextField suggestionFramePlayer = new JTextField("player");
	
	/**Text Field for user input of weapon in an suggestion/accusation*/
	JTextField suggestionFrameWeapon = new JTextField("weapon");
	
	/**JPanel containing the cancel and confirm buttons for the suggestion frame*/
	JPanel suggestionFrameButtons = new JPanel(new BorderLayout());
	
	/**Cancels the player's suggestion or accusation*/
	JButton suggestionFrameCancel = new JButton("Cancel");
	
	/**Submits the player's suggestion*/
	JButton suggestionFrameEnter = new JButton("Enter");
	
	/**JFrame that asks how many players are going to play at start of the game*/
	JFrame numberOfPlayersFrame = new JFrame("Number Of Players");
	
	/**JFrame that contains elements to let player roll the die*/
	JFrame diceFrame = new JFrame("Roll Die");
	
	/**Text that tells player to roll die, then what they rolled*/
	JLabel diceNumber = new JLabel("Roll Die");
	
	/**Rolls the die and then closes the dieFrame*/
	JButton diceButton = new JButton("Roll");
	
	/**JPanel that holds the list of cards the current player has*/
	JPanel cardHolder = new JPanel(new FlowLayout(FlowLayout.LEFT,5,0));
	
	/**JPanel that holds the list of cards in the current player's notebook*/
	JPanel notebookHolder = new JPanel(new FlowLayout(FlowLayout.LEFT,5,0));
	
	/**JFrame that tells the player they can make a suggestion*/
	JFrame canMakeSuggestionFrame = new JFrame();
	
	/**JLabel of text that tells player they can end their turn or roll the die
	 * depending on when the suggestion frame pops up
	 */
	JLabel canMakeSuggestionText2 = new JLabel("or end your turn");
	
	/**JFrame that contains text telling player who proved their suggestion false*/
	JFrame suggestionProofFrame = new JFrame();
	
	/**Text telling player who proved their suggestion false, split in two for space reasons*/
	JLabel suggestionProofText = new JLabel();

	/**Text telling player who proved their suggestion false, split in two for space reasons*/
	JLabel suggestionProofText2 = new JLabel();
	
	/**JFrame that tells player the input an invalid value for their suggestion*/
	JFrame errorFrame = new JFrame();
	
	/**Text telling player which input was invalid*/
	JLabel errorText = new JLabel();
	
	/**Text telling player valid inputs allowed*/
	JLabel errorText2 = new JLabel();
	
	/**Closes the errorFrame*/
	JButton errorButton = new JButton("Ok");
	
	/**JFrame containing the results of a player's accusation*/
	JFrame accusationResultFrame = new JFrame();
	
	/**Text displaying whether a player's accusation was correct or not*/
	JLabel accusationResultText = new JLabel();
	
	/**Button that closes the accusationResultFrame*/
	JButton accusationResultButton = new JButton("Ok");
	
	/**JFrame telling the players no one can play anymore*/
	JFrame noPlayersFrame = new JFrame();
	
	/**Text telling players no one can play*/
	JLabel noPlayersText = new JLabel("No one can play");
	
	/**JButton that quits the game when no one can play*/
	JButton noPlayersButton = new JButton("End Game");
	
	/**
	 * Main program for GUI, sets up all the details of every frame,button,label, etc.
	 */
	public Gui(){
		board.setLayout(null);
		board.setBackground(new Color(0,0,0));
		img.setBounds(0, 0, 800, 798);
		bg.add(img);
		bg.setBounds(0, 0, 1000, 1000);
		p1.setBackground(new Color(255,0,0));
		p1.setBounds(55 + (29 * 16), 38 + (29 * 0), 20, 20);
		p1.setBorder(BorderFactory.createLineBorder(new Color(0,0,0), 3));
		p2.setBackground(new Color(255,0,255));
		p2.setBounds(55 + (29 * 0), 38 + (29 * 5), 20, 20);
		p2.setBorder(BorderFactory.createLineBorder(new Color(0,0,0), 3));
		p3.setBackground(new Color(0,255,0));
		p3.setBounds(55 + (29 * 9), 38 + (29 * 24), 20, 20);
		p3.setBorder(BorderFactory.createLineBorder(new Color(0,0,0), 3));
		p4.setBackground(new Color(255,255,255));
		p4.setBounds(55 + (29 * 14), 38 + (29 * 24), 20, 20);
		p4.setBorder(BorderFactory.createLineBorder(new Color(0,0,0), 3));
		p5.setBackground(new Color(0,255,255));
		p5.setBounds(55 + (29 * 0), 38 + (29 * 18), 20, 20);
		p5.setBorder(BorderFactory.createLineBorder(new Color(0,0,0), 3));
		p6.setBackground(new Color(255,255,0));
		p6.setBounds(55 + (29 * 23), 38 + (29 * 7), 20, 20);
		p6.setBorder(BorderFactory.createLineBorder(new Color(0,0,0), 3));
		//starting position of first col is 55
		//starting position of first row is 38
		//move right or left is 29 pixels, up or down is 29 pixels
		board.add(p1);
		board.add(p2);
		board.add(p3);
		board.add(p4);
		board.add(p5);
		board.add(p6);
		board.add(bg);
		
		btns.setBackground(new Color(0,0,255));
		btns.setPreferredSize(new Dimension(200,200));
		btns.setLayout(new BorderLayout());
		left.setPreferredSize(new Dimension(250,250));
		right.setPreferredSize(new Dimension(250,250));
		up.setPreferredSize(new Dimension(500,75));
		down.setPreferredSize(new Dimension(500,75));
		makeSuggestion.setPreferredSize(new Dimension(330,50));
		makeAccusation.setPreferredSize(new Dimension(330,50));
		endTurn.setPreferredSize(new Dimension(340,50));
		suggestAccuseBtns.setPreferredSize(new Dimension(1000,50));
		suggestAccuseBtns.add(makeAccusation, BorderLayout.WEST);
		suggestAccuseBtns.add(makeSuggestion, BorderLayout.EAST);
		suggestAccuseBtns.add(endTurn);
		upDownBtns.add(up,BorderLayout.NORTH);
		upDownBtns.add(down,BorderLayout.SOUTH);
		btns.add(left,BorderLayout.WEST);
		btns.add(right,BorderLayout.EAST);
		btns.add(upDownBtns,BorderLayout.CENTER);
		btns.add(suggestAccuseBtns, BorderLayout.NORTH);
		
		left.setActionCommand("left");
		right.setActionCommand("right");
		up.setActionCommand("up");
		down.setActionCommand("down");
		makeSuggestion.setActionCommand("suggestion");
		makeAccusation.setActionCommand("accusation");
		endTurn.setActionCommand("endTurn");
		
		makeSuggestion.setEnabled(false);
		
		left.addActionListener(this);
		right.addActionListener(this);
		up.addActionListener(this);
		down.addActionListener(this);
		makeSuggestion.addActionListener(this);
		makeAccusation.addActionListener(this);
		endTurn.addActionListener(this);
		
		text.setLayout(new FlowLayout(FlowLayout.LEFT,5,5));
		text.setBackground(new Color(255,255,204));
		text.setPreferredSize(new Dimension(200,200));
		name.setPreferredSize(new Dimension(200,20));
		diceRoll.setPreferredSize(new Dimension(200,20));
		cards.setPreferredSize(new Dimension(200,20));
		notebook.setPreferredSize(new Dimension(200,20));
		cardHolder.setPreferredSize(new Dimension(200,20));
		notebookHolder.setPreferredSize(new Dimension(200,20));
		name.setFont(new Font("Arial",Font.BOLD,16));
		diceRoll.setFont(new Font("Arial",Font.BOLD,16));
		cards.setFont(new Font("Arial",Font.BOLD,16));
		notebook.setFont(new Font("Arial",Font.BOLD,16));
		text.add(name);
		text.add(diceRoll);
		text.add(cards);
		text.add(cardHolder);
		text.add(cardToggle);
		text.add(notebook);
		text.add(notebookHolder);
		text.add(notebookToggle);
		
		cards.setVisible(false);
		notebook.setVisible(false);
		
		cardToggle.setActionCommand("cards");
		notebookToggle.setActionCommand("notebook");
		
		cardToggle.addActionListener(this);
		notebookToggle.addActionListener(this);
		
		frame.setLayout(new BorderLayout());
		frame.setSize(1005,1035);
		frame.setResizable(false);
		frame.add(board,BorderLayout.CENTER);
		frame.add(btns,BorderLayout.SOUTH);
		frame.add(text,BorderLayout.EAST);
		frame.setVisible(true);
		
		suggestionFrame.setSize(500, 300);
		suggestionFrame.setResizable(false);
		suggestionFrame.setAlwaysOnTop(true);
		suggestionFrame.setDefaultCloseOperation(0);
		suggestionFrame.setLayout(new BorderLayout());
		suggestionFrame.add(suggestionFrameButtons, BorderLayout.SOUTH);
		suggestionFrame.add(suggestionFrameInputs);
		
		suggestionFrameRoom.setPreferredSize(new Dimension(500,50));
		suggestionFramePlayer.setPreferredSize(new Dimension(500,50));
		suggestionFrameWeapon.setPreferredSize(new Dimension(500,50));
		suggestionFrameButtons.setPreferredSize(new Dimension(500,50));
		suggestionFrameCancel.setPreferredSize(new Dimension(250,50));
		suggestionFrameEnter.setPreferredSize(new Dimension(250,50));
		
		suggestionFrameCancel.setActionCommand("suggestionCancel");
		suggestionFrameEnter.setActionCommand("suggestionEnter");
		
		suggestionFrameCancel.addActionListener(this);
		suggestionFrameEnter.addActionListener(this);
		
		suggestionFrameInputs.add(suggestionFrameRoom);
		suggestionFrameInputs.add(suggestionFramePlayer);
		suggestionFrameInputs.add(suggestionFrameWeapon);
		suggestionFrameButtons.add(suggestionFrameCancel, BorderLayout.WEST);
		suggestionFrameButtons.add(suggestionFrameEnter, BorderLayout.EAST);
		
		numberOfPlayersFrame.setLayout(new FlowLayout(FlowLayout.CENTER,0,0));
		numberOfPlayersFrame.setSize(410,135);
		numberOfPlayersFrame.setResizable(false);
		numberOfPlayersFrame.setDefaultCloseOperation(0);
		numberOfPlayersFrame.setAlwaysOnTop(true);
		openFrame(numberOfPlayersFrame);
		
		JButton three = new JButton("3");
		JButton four = new JButton("4");
		JButton five = new JButton("5");
		JButton six = new JButton("6");
		
		three.setPreferredSize(new Dimension(100,100));
		four.setPreferredSize(new Dimension(100,100));
		five.setPreferredSize(new Dimension(100,100));
		six.setPreferredSize(new Dimension(100,100));
		
		three.setActionCommand("3");
		four.setActionCommand("4");
		five.setActionCommand("5");
		six.setActionCommand("6");
		
		three.addActionListener(this);
		four.addActionListener(this);
		five.addActionListener(this);
		six.addActionListener(this);
		
		numberOfPlayersFrame.add(three);
		numberOfPlayersFrame.add(four);
		numberOfPlayersFrame.add(five);
		numberOfPlayersFrame.add(six);
		
		diceFrame.setSize(100, 150);
		diceFrame.setResizable(false);
		diceFrame.setAlwaysOnTop(true);
		diceFrame.setDefaultCloseOperation(0);
		diceFrame.setLayout(new BorderLayout());
		
		diceNumber.setPreferredSize(new Dimension(100,75));
		diceNumber.setFont(new Font("Arial",Font.BOLD,16));
		diceNumber.setHorizontalAlignment(SwingConstants.CENTER);
		
		diceButton.setActionCommand("roll");
		diceButton.addActionListener(this);
		diceButton.setPreferredSize(new Dimension(100,75));
		
		diceFrame.add(diceNumber, BorderLayout.NORTH);
		diceFrame.add(diceButton);
		
		canMakeSuggestionFrame.setSize(300,200);
		canMakeSuggestionFrame.setAlwaysOnTop(true);
		canMakeSuggestionFrame.setLayout(new BorderLayout());
		canMakeSuggestionFrame.setResizable(false);
		canMakeSuggestionFrame.setDefaultCloseOperation(0);
		
		JPanel canMakeSuggestionTextHolder = new JPanel(new BorderLayout());
		canMakeSuggestionTextHolder.setPreferredSize(new Dimension(300,200));
		
		JLabel canMakeSuggestionText = new JLabel("You can now make a suggestion");
		canMakeSuggestionText.setPreferredSize(new Dimension(300,100));
		canMakeSuggestionText.setFont(new Font("Arial",Font.BOLD,16));
		canMakeSuggestionText.setHorizontalAlignment(SwingConstants.CENTER);
		
		canMakeSuggestionText2.setPreferredSize(new Dimension(300,100));
		canMakeSuggestionText2.setFont(new Font("Arial",Font.BOLD,16));
		canMakeSuggestionText2.setHorizontalAlignment(SwingConstants.CENTER);
		
		JButton canMakeSuggestionButton = new JButton("Ok");
		canMakeSuggestionButton.setPreferredSize(new Dimension(300,50));
		canMakeSuggestionButton.setActionCommand("makeSuggestionOk");
		canMakeSuggestionButton.addActionListener(this);
		
		canMakeSuggestionTextHolder.add(canMakeSuggestionText, BorderLayout.NORTH);
		canMakeSuggestionTextHolder.add(canMakeSuggestionText2, BorderLayout.SOUTH);
		canMakeSuggestionFrame.add(canMakeSuggestionButton, BorderLayout.SOUTH);
		canMakeSuggestionFrame.add(canMakeSuggestionTextHolder);
		
		suggestionProofFrame.setSize(400,200);
		suggestionProofFrame.setAlwaysOnTop(true);
		suggestionProofFrame.setLayout(new BorderLayout());
		suggestionProofFrame.setResizable(false);
		suggestionProofFrame.setDefaultCloseOperation(0);
		
		JPanel suggestionProofTextHolder = new JPanel(new BorderLayout());
		suggestionProofTextHolder.setPreferredSize(new Dimension(400,200));
		
		suggestionProofText.setPreferredSize(new Dimension(400,100));
		suggestionProofText.setFont(new Font("Arial",Font.BOLD,16));
		suggestionProofText.setHorizontalAlignment(SwingConstants.CENTER);
		suggestionProofTextHolder.add(suggestionProofText, BorderLayout.NORTH);
		
		suggestionProofText2.setPreferredSize(new Dimension(400,100));
		suggestionProofText2.setFont(new Font("Arial",Font.BOLD,16));
		suggestionProofText2.setHorizontalAlignment(SwingConstants.CENTER);
		suggestionProofTextHolder.add(suggestionProofText2, BorderLayout.SOUTH);
		
		JButton suggestionProofButton = new JButton("Ok");
		suggestionProofButton.setPreferredSize(new Dimension(400,50));
		suggestionProofButton.setActionCommand("suggestionProofOk");
		suggestionProofButton.addActionListener(this);
		suggestionProofFrame.add(suggestionProofButton, BorderLayout.SOUTH);
		suggestionProofFrame.add(suggestionProofTextHolder);
		
		errorFrame.setSize(800,200);
		errorFrame.setLayout(new BorderLayout());
		errorFrame.setResizable(false);
		errorFrame.setAlwaysOnTop(true);
		errorFrame.setDefaultCloseOperation(0);
		
		JPanel errorTextHolder = new JPanel(new BorderLayout());
		errorTextHolder.setPreferredSize(new Dimension(800,100));
		
		errorText.setFont(new Font("Arial",Font.BOLD,16));
		errorText.setHorizontalAlignment(SwingConstants.CENTER);
		
		errorText2.setFont(new Font("Arial",Font.BOLD,16));
		errorText2.setHorizontalAlignment(SwingConstants.CENTER);
		errorText2.setPreferredSize(new Dimension(800,50));
		
		errorTextHolder.add(errorText);
		errorTextHolder.add(errorText2, BorderLayout.SOUTH);
		
		errorButton.setPreferredSize(new Dimension(800,50));
		errorButton.setActionCommand("errorOk");
		errorButton.addActionListener(this);
		
		errorFrame.add(errorTextHolder);
		errorFrame.add(errorButton, BorderLayout.SOUTH);
		
		accusationResultFrame.setSize(400,200);
		accusationResultFrame.setAlwaysOnTop(true);
		accusationResultFrame.setLayout(new BorderLayout());
		accusationResultFrame.setResizable(false);
		accusationResultFrame.setDefaultCloseOperation(0);
		
		accusationResultButton.setPreferredSize(new Dimension(400,50));
		accusationResultButton.setActionCommand("accusationResultOk");
		accusationResultButton.addActionListener(this);
		
		accusationResultText.setPreferredSize(new Dimension(400,100));
		accusationResultText.setFont(new Font("Arial",Font.BOLD,16));
		accusationResultText.setHorizontalAlignment(SwingConstants.CENTER);
		
		accusationResultFrame.add(accusationResultButton,BorderLayout.SOUTH);
		accusationResultFrame.add(accusationResultText);
		
		noPlayersFrame.setSize(400,200);
		noPlayersFrame.setAlwaysOnTop(true);
		noPlayersFrame.setLayout(new BorderLayout());
		noPlayersFrame.setResizable(false);
		noPlayersFrame.setDefaultCloseOperation(0);
		
		noPlayersButton.setPreferredSize(new Dimension(400,50));
		noPlayersButton.setActionCommand("noPlayersOk");
		noPlayersButton.addActionListener(this);
		
		noPlayersText.setPreferredSize(new Dimension(400,100));
		noPlayersText.setFont(new Font("Arial",Font.BOLD,16));
		noPlayersText.setHorizontalAlignment(SwingConstants.CENTER);
		
		noPlayersFrame.add(noPlayersButton,BorderLayout.SOUTH);
		noPlayersFrame.add(noPlayersText);

	}

	/**
	 * Overiden method from ActionListener, listens for button clicks and activates the
	 * correct block of code within the switch statement based on the action command of
	 * the button pressed.
	 * @param e - ActionEvent from the button pressed
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		JPanel tmp = null;
		switch(e.getActionCommand()){
		case "left"://move left button
			tmp = getToken(cluedoMain.currentPlayer);
			if(cluedoMain.currentPlayer.canMakeASuggestion()){
				cluedoMain.currentPlayer.setSuggestion(false);
				makeSuggestion.setEnabled(false);
			}
			if(cluedoMain.currentPlayer.move("left")){
				y = cluedoMain.currentPlayer.getPos()[1];
				x = cluedoMain.currentPlayer.getPos()[0];			
			}
			break;
		case "right"://move right button
			tmp = getToken(cluedoMain.currentPlayer);
			if(cluedoMain.currentPlayer.canMakeASuggestion()){
				cluedoMain.currentPlayer.setSuggestion(false);
				makeSuggestion.setEnabled(false);
			}
			if(cluedoMain.currentPlayer.move("right")){
				y = cluedoMain.currentPlayer.getPos()[1];
				x = cluedoMain.currentPlayer.getPos()[0];			
			}
			break;
		case "up"://move up button
			tmp = getToken(cluedoMain.currentPlayer);
			if(cluedoMain.currentPlayer.canMakeASuggestion()){
				cluedoMain.currentPlayer.setSuggestion(false);
				makeSuggestion.setEnabled(false);
			}
			if(cluedoMain.currentPlayer.move("up")){
				y = cluedoMain.currentPlayer.getPos()[1];
				x = cluedoMain.currentPlayer.getPos()[0];
			}
			break;
		case "down"://move down button
			tmp = getToken(cluedoMain.currentPlayer);
			if(cluedoMain.currentPlayer.canMakeASuggestion()){
				cluedoMain.currentPlayer.setSuggestion(false);
				makeSuggestion.setEnabled(false);
			}
			if(cluedoMain.currentPlayer.move("down")){
				y = cluedoMain.currentPlayer.getPos()[1];
				x = cluedoMain.currentPlayer.getPos()[0];			
			}
			break;
		case "cards"://show/hide cards button
			cards.setVisible(!cards.isVisible());
			cardHolder.setVisible(!cardHolder.isVisible());
			break;
		case "notebook"://show/hide notebook button
			notebook.setVisible(!notebook.isVisible());
			notebookHolder.setVisible(!notebookHolder.isVisible());
			break;
		case "suggestion"://"make a suggestion" button
			suggestionFrame.setTitle("Make A Suggestion");
			suggestionFrameRoom.setText(((Room)cluedoMain.currentPlayer.getCurrentSpace()).getName());
			suggestionFrameRoom.setEditable(false);
			suggestionFramePlayer.setText("player");
			suggestionFrameWeapon.setText("weapon");
			openFrame(suggestionFrame);
			break;
		case "accusation"://"make an suggestion" button
			suggestionFrame.setTitle("Make An Accusation");
			suggestionFrameRoom.setEditable(true);
			suggestionFrameRoom.setText("room");
			suggestionFramePlayer.setText("player");
			suggestionFrameWeapon.setText("weapon");
			openFrame(suggestionFrame);
			break;
		case "suggestionCancel"://cancel suggestion button, in suggestionFrame
			closeFrame(suggestionFrame);
			break;
		case "suggestionEnter"://enter suggestion button, in suggestionFrame
			String room = suggestionFrameRoom.getText();
			String player = suggestionFramePlayer.getText();
			String weapon = suggestionFrameWeapon.getText();
			if(suggestionFrame.getTitle() == "Make A Suggestion"){
				if(!cluedoMain.allPlayerCards.contains(player)){
					openFrame(errorFrame);
					suggestionFrame.setEnabled(false);
					errorText.setText("Invalid Player, valid selections are:");
					errorText2.setText("Mrs. Scarlett, Col. Mustard, Mrs. White, Mr. Green, Mrs. Peacock, Prof. Plum");
				}else if(!cluedoMain.allWeaponCards.contains(weapon)){
					openFrame(errorFrame);
					suggestionFrame.setEnabled(false);
					errorText.setText("Invalid Weapon, valid selections are:");
					errorText2.setText("Candle Stick, Knife, Revolver, Rope, Lead Pipe, Wrench");
				}else{
					if(cluedoMain.diceRoll != 0){
						left.setEnabled(false);
						right.setEnabled(false);
						up.setEnabled(false);
						down.setEnabled(false);
						cluedoMain.diceRoll = 0;
						setDie(0);
					}
					closeFrame(suggestionFrame);
					LinkedList<String> results = cluedoMain.currentPlayer.suggestionProof(new String[]{room,player,weapon}, cluedoMain.playerQueue.toArray());
					moveSuggestedPlayer(player,room);
					makeSuggestion.setEnabled(false);
					if(results.isEmpty()){
						suggestionProofText.setText("No one proved your suggestion false,");
						suggestionProofText2.setText("you may now make an accusation or end your turn");
						openFrame(suggestionProofFrame);
					}else{
						suggestionProofText.setText(results.get(0) + " proved your suggestion ");
						suggestionProofText2.setText("false with '" + results.get(1) +"'");
						cluedoMain.currentPlayer.addToNotebook(results.get(1));
						setNotebook(cluedoMain.currentPlayer);
						openFrame(suggestionProofFrame);
					}
				}
			}else if(suggestionFrame.getTitle() == "Make An Accusation"){
				if(!cluedoMain.allRoomCards.contains(room)){
					openFrame(errorFrame);
					suggestionFrame.setEnabled(false);
					errorText.setText("Invalid Room, valid selections are:");
					errorText2.setText("Hall, Ballroom, Conservatory, Kitchen, Billiard Room, Study, Dining Room, Library, Lounge");
				}else if(!cluedoMain.allPlayerCards.contains(player)){
					openFrame(errorFrame);
					suggestionFrame.setEnabled(false);
					errorText.setText("Invalid Player, valid selections are:");
					errorText2.setText("Mrs. Scarlett, Col. Mustard, Mrs. White, Mr. Green, Mrs. Peacock, Prof. Plum");
				}else if(!cluedoMain.allWeaponCards.contains(weapon)){
					openFrame(errorFrame);
					suggestionFrame.setEnabled(false);
					errorText.setText("Invalid Weapon, valid selections are:");
					errorText2.setText("Candle Stick, Knife, Revolver, Rope, Lead Pipe, Wrench");
				}else{
					closeFrame(suggestionFrame);
					if(!room.toLowerCase().equals(cluedoMain.cardsInEnvelope[0].toLowerCase())){
						loseGame();
					}else if(!player.toLowerCase().equals(cluedoMain.cardsInEnvelope[1].toLowerCase())){
						loseGame();
					}else if(!weapon.toLowerCase().equals(cluedoMain.cardsInEnvelope[2].toLowerCase())){
						loseGame();
					}else{
						winGame();
					}
				}
			}
			break;
		case "makeSuggestionOk"://"ok" button when suggestion can be made, in canMakeSuggestionFrame
			makeSuggestion.setEnabled(true);
			if(canMakeSuggestionText2.getText() == "or end your turn"){
				left.setEnabled(false);
				right.setEnabled(false);
				up.setEnabled(false);
				down.setEnabled(false);
				closeFrame(canMakeSuggestionFrame);
			}else{
				closeFrame(canMakeSuggestionFrame);
				rollDie();
			}
			break;
		case "suggestionProofOk"://"ok" button after suggestion made, in suggestionProofFrame
			setNotebook(cluedoMain.currentPlayer);
			closeFrame(suggestionProofFrame);
			break;
		case "errorOk"://"ok" button after suggestion input invalid, in errorFrame
			suggestionFrame.setEnabled(true);
			closeFrame(errorFrame);
			break;
		case "3"://3 players input button, in numberOfPlayersFrame
			cluedoMain.run(3);
			closeFrame(numberOfPlayersFrame);
			break;
		case "4"://4 players input button, in numberOfPlayersFrame
			cluedoMain.run(4);
			closeFrame(numberOfPlayersFrame);
			break;
		case "5"://5 players input button, in numberOfPlayersFrame
			cluedoMain.run(5);
			closeFrame(numberOfPlayersFrame);
			break;
		case "6"://6 players input button, in numberOfPlayersFrame
			cluedoMain.run(6);
			closeFrame(numberOfPlayersFrame);
			break;
		case "roll"://"roll die" button before die roll, in diceFrame
			int i = new Random().nextInt(6) + 1; // 1-6 inclusive
			cluedoMain.diceRoll = i;
			diceNumber.setText("You Rolled A " + i + "!");
			diceButton.setText("Enter");
			diceButton.setActionCommand("rollEnter");
			setDie(i);
			break;
		case "rollEnter"://"enter" button after die roll, in diceFrame
			diceButton.setText("Roll");
			diceButton.setActionCommand("roll");
			diceNumber.setText("Roll Die");
			closeFrame(diceFrame);
			break;
		case "endTurn"://end turn button
			cluedoMain.diceRoll = 0;
			cluedoMain.currentPlayer.setSuggestion(false);
			setDie(0);
			cluedoMain.nextPlayer();
			break;
		case "accusationResultOk"://accusationResult ok button
			closeFrame(accusationResultFrame);
			if(accusationResultText.getText() == "Your accusation was incorrect, you lose!"){
				cluedoMain.diceRoll = 0;
				cluedoMain.currentPlayer.setSuggestion(false);
				setDie(0);
				cluedoMain.nextPlayer();
			}else{
				System.exit(0);
			}
			break;
		case "noPlayersOk"://ok button on noPlayersFrame
			System.exit(0);
			break;
		}
		if(tmp != null){
			setDie(cluedoMain.diceRoll);
			tmp.setBounds(55 + (29 * x), 38 + (29 * y), 20, 20);
			if(cluedoMain.diceRoll == 0){
				if(cluedoMain.currentPlayer.canMakeASuggestion()){
					openSuggestionText("end");
				}else{
					cluedoMain.nextPlayer();
				}
			}
		}
	}
		
	/**
	 * Returns the token representing the player passed in based on the players name
	 * @param p - the player whose token is being looked for
	 * @return the JPanel token that represents the player
	 */
	private JPanel getToken(Player p){
		switch(p.getName()){
		case "Mrs. Scarlett":
			return p1;
		case "Prof. Plum":
			return p2;
		case "Mr. Green":
			return p3;
		case "Mrs. White":
			return p4;
		case "Mrs. Peacock":
			return p5;
		case "Col. Mustard":
			return p6;
		}
		return null; //error case
	}
	
	/**
	 * Opens frames so they open in the center of the game screen and freeze the main
	 * game frame
	 * @param f - the frame to be opened
	 */
	private void openFrame(Frame f){
		f.setLocation(frame.getLocationOnScreen().x + frame.getHeight() / 4, frame.getLocationOnScreen().y + frame.getWidth() / 4);
		f.setVisible(true);
		frame.setEnabled(false);
	}
	
	/**
	 * Closes frames and unlocks the main game frame
	 * @param f - the frame to be closed
	 */
	private void closeFrame(Frame f){
		f.setVisible(false);
		frame.setAlwaysOnTop(true);
		frame.setAlwaysOnTop(false);
		if(!(f.getTitle() == "Number Of Players")){
			frame.setEnabled(true);
		}
	}
	
	/**
	 * Sets the "Name:" Jpanel in the right text panel to that of the player passed
	 * @param p - the player whose name you want
	 */
	public void setName(Player p){
		name.setText("Name:" + p.getName());
	}
	
	/**
	 * Sets the "Card:" Jpanel in the right text panel to those that the player contains
	 * and resizes it to allow all the cards to be visible
	 * @param p - the player whose cards you want
	 */
	public void setCards(Player p){
		cardHolder.setVisible(false);
		cards.setVisible(false);
		cardHolder.removeAll();
		for(String s: p.getCards()){
			JLabel tmp = new JLabel(s);
			tmp.setPreferredSize(new Dimension(200,20));
			tmp.setFont(new Font("Arial",Font.BOLD,14));
			cardHolder.add(tmp);
		}
		cardHolder.setPreferredSize(new Dimension(200,p.getCards().size() * 20));
	}
	
	/**
	 * Sets the "Notebook:" Jpanel in the right text panel to cards the player contains
	 * in their notebook and resizes it to allow all the cards to be visible
	 * @param p - the player whose notebook you want
	 */
	public void setNotebook(Player p){
		notebookHolder.setVisible(false);
		notebook.setVisible(false);
		notebookHolder.removeAll();
		for(String s: p.getNotebook()){
			JLabel tmp = new JLabel(s);
			tmp.setPreferredSize(new Dimension(200,20));
			tmp.setFont(new Font("Arial",Font.BOLD,14));
			notebookHolder.add(tmp);
		}
		notebookHolder.setPreferredSize(new Dimension(200,p.getNotebook().size() * 20));
	}
	
	/**
	 * Opens the diceFrame, used by the cluedoMain class
	 */
	public void rollDie(){
		openFrame(diceFrame);
	}
	
	/**
	 * Sets the "Moves Left:" Jpanel in the right text panel to the number of move the
	 * player has left
	 * @param n -the number of moves the player has left
	 */
	public void setDie(int n){
		diceRoll.setText("Moves Left: " + n);
	}
	
	/**
	 * Moves the player who was accused in a suggestion to the room the suggestion was made
	 * in and updates that player so that they can make a suggestion at the start of their
	 * turn
	 * @param playerName - name of the player to be moved
	 * @param room - name of the room the player is to be moved to
	 */
	private void moveSuggestedPlayer(String playerName, String room){
		if(!cluedoMain.currentPlayer.getName().toLowerCase().equals(playerName.toLowerCase())){
			Player tmp = new Player();
			for(Object o: cluedoMain.playerQueue.toArray()){
				String curName = ((Player)o).getName().toLowerCase();
				if(curName.equals(playerName.toLowerCase())){
					tmp = (Player)o;
				}
			}
			tmp.moveBySuggestion = true;
			switch(room.toLowerCase()){
			case "study":
				tmp.moveToNewRoom(1,1);
				break;
			case "hall":
				tmp.moveToNewRoom(10,1);
				break;
			case "lounge":
				tmp.moveToNewRoom(18,2);
				break;
			case "library":
				tmp.moveToNewRoom(1,7);
				break;
			case "dining room":
				tmp.moveToNewRoom(19,10);
				break;
			case "billiard room":
				tmp.moveToNewRoom(1,13);
				break;
			case "conservatory":
				tmp.moveToNewRoom(1,21);
				break;
			case "ballroom":
				tmp.moveToNewRoom(10,20);
				break;
			case "kitchen":
				tmp.moveToNewRoom(20,20);
				break;
			}
			tmp.moveBySuggestion = false;
			tmp.setSuggestion(true);
			JPanel token = getToken(tmp);
			int y = tmp.getPos()[1];
			int x = tmp.getPos()[0];
			token.setBounds(55 + (29 * x), 38 + (29 * y), 20, 20);
		}
	}
	
	/**
	 * Opens the frame that tells the player they can now make a suggestion. Changes the
	 * text within the frame based on when the player is told they can make a suggestion
	 * @param s - String representing when the player is told they can make a suggestion
	 */
	public void openSuggestionText(String s){
		if(s == "end"){
			canMakeSuggestionText2.setText("or end your turn");
		}else if (s == "start"){
			canMakeSuggestionText2.setText("or move your token");
		}
		openFrame(canMakeSuggestionFrame);
	}
	
	/**
	 * Called when player makes an incorrect accusation. Tells the player they lost
	 * and sets that players lost boolean to true;
	 */
	public void loseGame(){
		accusationResultText.setText("Your accusation was incorrect, you lose!");
		cluedoMain.currentPlayer.lost();
		openFrame(accusationResultFrame);
	}
	
	/**
	 * Called when player makes a correct accusation. Tells the player they won and
	 * brings up the frame that will end the game.
	 */
	public void winGame(){
		accusationResultButton.setText("End Game");
		accusationResultText.setText("Congrats, you win!");
		openFrame(accusationResultFrame);
	}
	
	/**
	 * Called from cluedoMain when no one can player anymore, opens the frame that
	 * will end the game.
	 */
	public void endGame(){
		openFrame(noPlayersFrame);
	}
}
