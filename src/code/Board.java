package code;

/**
 * This class contains methods that represent gameplay elements for the board object. Methods and
 * fields are static because only one board will be made per game. Contains constructor that places
 * all tiles on the board.
 * @author Sean Rowland, Jared Brown
 *
 */

public class Board {

	/**2-D array that holds Objects that represent tiles on the board*/
	static Object[][] b;
	
	/** Constructor method for the static Board object. Goes through and sets all the spaces
	 * on the board, the 2D array b, to the correct objects. Should only be called
	 * once at the start of the game.
	 */
	public Board(){
		b = new Object[24][25];
		for(int i =0; i < b.length; i++){
			for(int j = 0; j < b[i].length; j++){
				b[i][j] = new Hallway();
			}
		}
		//Study
		b[0][0] = new SecretPassage(0);
		b[0][1] = new Room("Study");
		b[0][2] = new Room("Study");
		b[0][3] = new Room("Study");
		b[1][0] = new Room("Study");
		b[1][1] = new Room("Study");
		b[1][2] = new Room("Study");
		b[1][3] = new Room("Study");
		b[2][0] = new Room("Study");
		b[2][1] = new Room("Study");
		b[2][2] = new Room("Study");
		b[2][3] = new Room("Study");
		b[3][0] = new Room("Study");
		b[3][1] = new Room("Study");
		b[3][2] = new Room("Study");
		b[3][3] = new Room("Study");
		b[4][0] = new Room("Study");
		b[4][1] = new Room("Study");
		b[4][2] = new Room("Study");
		b[4][3] = new Room("Study");
		b[5][0] = new Room("Study");
		b[5][1] = new Room("Study");
		b[5][2] = new Room("Study");
		b[5][3] = new Room("Study");
		b[6][0] = new Room("Study");
		b[6][1] = new Room("Study");
		b[6][2] = new Room("Study");
		b[6][3] = new Door(0);
		
		//Hall
		b[9][0] = new Room("Hall");
		b[9][1] = new Room("Hall");
		b[9][2] = new Room("Hall");
		b[9][3] = new Room("Hall");
		b[9][4] = new Door(1);
		b[9][5] = new Room("Hall");
		b[9][6] = new Room("Hall");
		
		b[10][0] = new Room("Hall");
		b[10][1] = new Room("Hall");
		b[10][2] = new Room("Hall");
		b[10][3] = new Room("Hall");
		b[10][4] = new Room("Hall");
		b[10][5] = new Room("Hall");
		b[10][6] = new Room("Hall");
		
		b[11][0] = new Room("Hall");
		b[11][1] = new Room("Hall");
		b[11][2] = new Room("Hall");
		b[11][3] = new Room("Hall");
		b[11][4] = new Room("Hall");
		b[11][5] = new Room("Hall");
		b[11][6] = new Door(1);
		
		b[12][0] = new Room("Hall");
		b[12][1] = new Room("Hall");
		b[12][2] = new Room("Hall");
		b[12][3] = new Room("Hall");
		b[12][4] = new Room("Hall");
		b[12][5] = new Room("Hall");
		b[12][6] = new Door(1);
		
		b[13][0] = new Room("Hall");
		b[13][1] = new Room("Hall");
		b[13][2] = new Room("Hall");
		b[13][3] = new Room("Hall");
		b[13][4] = new Room("Hall");
		b[13][5] = new Room("Hall");
		b[13][6] = new Room("Hall");
		
		b[14][0] = new Room("Hall");
		b[14][1] = new Room("Hall");
		b[14][2] = new Room("Hall");
		b[14][3] = new Room("Hall");
		b[14][4] = new Room("Hall");
		b[14][5] = new Room("Hall");
		b[14][6] = new Room("Hall");
		
		//Lounge
		b[17][1] = new Room("Lounge");
		b[17][2] = new Room("Lounge");
		b[17][3] = new Room("Lounge");
		b[17][4] = new Room("Lounge");
		b[17][5] = new Door(2);
		
		b[18][0] = new Room("Lounge");
		b[18][1] = new Room("Lounge");
		b[18][2] = new Room("Lounge");
		b[18][3] = new Room("Lounge");
		b[18][4] = new Room("Lounge");
		b[18][5] = new Room("Lounge");
	
		b[19][0] = new Room("Lounge");
		b[19][1] = new Room("Lounge");
		b[19][2] = new Room("Lounge");
		b[19][3] = new Room("Lounge");
		b[19][4] = new Room("Lounge");
		b[19][5] = new Room("Lounge");
		
		b[20][0] = new Room("Lounge");
		b[20][1] = new Room("Lounge");
		b[20][2] = new Room("Lounge");
		b[20][3] = new Room("Lounge");
		b[20][4] = new Room("Lounge");
		b[20][5] = new Room("Lounge");

		b[21][0] = new Room("Lounge");
		b[21][1] = new Room("Lounge");
		b[21][2] = new Room("Lounge");
		b[21][3] = new Room("Lounge");
		b[21][4] = new Room("Lounge");
		b[21][5] = new Room("Lounge");
		
		b[22][0] = new Room("Lounge");
		b[22][1] = new Room("Lounge");
		b[22][2] = new Room("Lounge");
		b[22][3] = new Room("Lounge");
		b[22][4] = new Room("Lounge");
		b[22][5] = new Room("Lounge");
		
		b[23][0] = new SecretPassage(1);
		b[23][1] = new Room("Lounge");
		b[23][2] = new Room("Lounge");
		b[23][3] = new Room("Lounge");
		b[23][4] = new Room("Lounge");
		b[23][5] = new Room("Lounge");
		
		//Library
		b[0][7] = new Room("Library");
		b[0][8] = new Room("Library");
		b[0][9] = new Room("Library");
		
		b[1][6] = new Room("Library");
		b[1][7] = new Room("Library");
		b[1][8] = new Room("Library");
		b[1][9] = new Room("Library");
		b[1][10] = new Room("Library");
		
		b[2][6] = new Room("Library");
		b[2][7] = new Room("Library");
		b[2][8] = new Room("Library");
		b[2][9] = new Room("Library");
		b[2][10] = new Room("Library");
		
		b[3][6] = new Room("Library");
		b[3][7] = new Room("Library");
		b[3][8] = new Room("Library");
		b[3][9] = new Room("Library");
		b[3][10] = new Door(3);
		
		b[4][6] = new Room("Library");
		b[4][7] = new Room("Library");
		b[4][8] = new Room("Library");
		b[4][9] = new Room("Library");
		b[4][10] = new Room("Library");
		
		b[5][6] = new Room("Library");
		b[5][7] = new Room("Library");
		b[5][8] = new Room("Library");
		b[5][9] = new Room("Library");
		b[5][10] = new Room("Library");
		
		b[6][7] = new Room("Library");
		b[6][8] = new Door(3);
		b[6][9] = new Room("Library");
		
		//Billiard Room
		b[0][12] = new Room("Billiard Room");
		b[0][13] = new Room("Billiard Room");
		b[0][14] = new Room("Billiard Room");
		b[0][15] = new Room("Billiard Room");
		b[0][16] = new Room("Billiard Room");
		
		b[1][12] = new Door(5);
		b[1][13] = new Room("Billiard Room");
		b[1][14] = new Room("Billiard Room");
		b[1][15] = new Room("Billiard Room");
		b[1][16] = new Room("Billiard Room");
		
		b[3][12] = new Room("Billiard Room");
		b[3][13] = new Room("Billiard Room");
		b[3][14] = new Room("Billiard Room");
		b[3][15] = new Room("Billiard Room");
		b[3][16] = new Room("Billiard Room");

		b[4][12] = new Room("Billiard Room");
		b[4][13] = new Room("Billiard Room");
		b[4][14] = new Room("Billiard Room");
		b[4][15] = new Room("Billiard Room");
		b[4][16] = new Room("Billiard Room");

		b[5][12] = new Room("Billiard Room");
		b[5][13] = new Room("Billiard Room");
		b[5][14] = new Room("Billiard Room");
		b[5][15] = new Door(5);
		b[5][16] = new Room("Billiard Room");

		//Conservatory
		b[0][20] = new Room("Conservatory");
		b[0][21] = new Room("Conservatory");
		b[0][22] = new Room("Conservatory");
		b[0][23] = new SecretPassage(2);
		
		b[1][19] = new Room("Conservatory");
		b[1][20] = new Room("Conservatory");
		b[1][21] = new Room("Conservatory");
		b[1][22] = new Room("Conservatory");
		b[1][23] = new Room("Conservatory");
		
		b[2][19] = new Room("Conservatory");
		b[2][20] = new Room("Conservatory");
		b[2][21] = new Room("Conservatory");
		b[2][22] = new Room("Conservatory");
		b[2][23] = new Room("Conservatory");
		
		b[3][19] = new Room("Conservatory");
		b[3][20] = new Room("Conservatory");
		b[3][21] = new Room("Conservatory");
		b[3][22] = new Room("Conservatory");
		b[3][23] = new Room("Conservatory");
		
		b[4][19] = new Door(6);
		b[4][20] = new Room("Conservatory");
		b[4][21] = new Room("Conservatory");
		b[4][22] = new Room("Conservatory");
		b[4][23] = new Room("Conservatory");

		b[5][20] = new Room("Conservatory");
		b[5][21] = new Room("Conservatory");
		b[5][22] = new Room("Conservatory");
		b[5][23] = new Room("Conservatory");
		
		//Dining Room
		b[16][9] = new Room("Dining Room");
		b[16][10] = new Room("Dining Room");
		b[16][11] = new Room("Dining Room");
		b[16][12] = new Door(4);
		b[16][13] = new Room("Dining Room");
		b[16][14] = new Room("Dining Room");
		
		b[17][9] = new Door(4);
		b[17][10] = new Room("Dining Room");
		b[17][11] = new Room("Dining Room");
		b[17][12] = new Room("Dining Room");
		b[17][13] = new Room("Dining Room");
		b[17][14] = new Room("Dining Room");
		
		b[18][9] = new Room("Dining Room");
		b[18][10] = new Room("Dining Room");
		b[18][11] = new Room("Dining Room");
		b[18][12] = new Room("Dining Room");
		b[18][13] = new Room("Dining Room");
		b[18][14] = new Room("Dining Room");
		
		b[19][9] = new Room("Dining Room");
		b[19][10] = new Room("Dining Room");
		b[19][11] = new Room("Dining Room");
		b[19][12] = new Room("Dining Room");
		b[19][13] = new Room("Dining Room");
		b[19][14] = new Room("Dining Room");
		b[19][15] = new Room("Dining Room");
	
		b[20][9] = new Room("Dining Room");
		b[20][10] = new Room("Dining Room");
		b[20][11] = new Room("Dining Room");
		b[20][12] = new Room("Dining Room");
		b[20][13] = new Room("Dining Room");
		b[20][14] = new Room("Dining Room");
		b[20][15] = new Room("Dining Room");
		
		b[21][9] = new Room("Dining Room");
		b[21][10] = new Room("Dining Room");
		b[21][11] = new Room("Dining Room");
		b[21][12] = new Room("Dining Room");
		b[21][13] = new Room("Dining Room");
		b[21][14] = new Room("Dining Room");
		b[21][15] = new Room("Dining Room");
	
		b[22][9] = new Room("Dining Room");
		b[22][10] = new Room("Dining Room");
		b[22][11] = new Room("Dining Room");
		b[22][12] = new Room("Dining Room");
		b[22][13] = new Room("Dining Room");
		b[22][14] = new Room("Dining Room");
		b[22][15] = new Room("Dining Room");
		
		b[23][9] = new Room("Dining Room");
		b[23][10] = new Room("Dining Room");
		b[23][11] = new Room("Dining Room");
		b[23][12] = new Room("Dining Room");
		b[23][13] = new Room("Dining Room");
		b[23][14] = new Room("Dining Room");
		b[23][15] = new Room("Dining Room");
		
		//Kitchen
		b[18][18] = new Room("Kitchen");
		b[18][19] = new Room("Kitchen");
		b[18][20] = new Room("Kitchen");
		b[18][21] = new Room("Kitchen");
		b[18][22] = new Room("Kitchen");
		b[18][23] = new Room("Kitchen");
		
		b[19][18] = new Door(8);
		b[19][19] = new Room("Kitchen");
		b[19][20] = new Room("Kitchen");
		b[19][21] = new Room("Kitchen");
		b[19][22] = new Room("Kitchen");
		b[19][23] = new Room("Kitchen");
		
		b[20][18] = new Room("Kitchen");
		b[20][19] = new Room("Kitchen");
		b[20][20] = new Room("Kitchen");
		b[20][21] = new Room("Kitchen");
		b[20][22] = new Room("Kitchen");
		b[20][23] = new Room("Kitchen");
		
		b[21][18] = new Room("Kitchen");
		b[21][19] = new Room("Kitchen");
		b[21][20] = new Room("Kitchen");
		b[21][21] = new Room("Kitchen");
		b[21][22] = new Room("Kitchen");
		b[21][23] = new Room("Kitchen");
		
		b[22][18] = new Room("Kitchen");
		b[22][19] = new Room("Kitchen");
		b[22][20] = new Room("Kitchen");
		b[22][21] = new Room("Kitchen");
		b[22][22] = new Room("Kitchen");
		b[22][23] = new Room("Kitchen");
		
		b[23][19] = new Room("Kitchen");
		b[23][20] = new Room("Kitchen");
		b[23][21] = new Room("Kitchen");
		b[23][22] = new Room("Kitchen");
		b[23][23] = new SecretPassage(3);
		
		//Ballroom
		b[8][17] = new Room("Ballroom");
		b[8][18] = new Room("Ballroom");
		b[8][19] = new Door(7);
		b[8][20] = new Room("Ballroom");
		b[8][21] = new Room("Ballroom");
		b[8][22] = new Room("Ballroom");
		
		b[9][17] = new Door(7);
		b[9][18] = new Room("Ballroom");
		b[9][19] = new Room("Ballroom");
		b[9][20] = new Room("Ballroom");
		b[9][21] = new Room("Ballroom");
		b[9][22] = new Room("Ballroom");
		
		b[10][17] = new Room("Ballroom");
		b[10][18] = new Room("Ballroom");
		b[10][19] = new Room("Ballroom");
		b[10][20] = new Room("Ballroom");
		b[10][21] = new Room("Ballroom");
		b[10][22] = new Room("Ballroom");
		b[10][23] = new Room("Ballroom");
		b[10][24] = new Room("Ballroom");

		b[11][17] = new Room("Ballroom");
		b[11][18] = new Room("Ballroom");
		b[11][19] = new Room("Ballroom");
		b[11][20] = new Room("Ballroom");
		b[11][21] = new Room("Ballroom");
		b[11][22] = new Room("Ballroom");
		b[11][23] = new Room("Ballroom");
		b[11][24] = new Room("Ballroom");
		
		b[12][17] = new Room("Ballroom");
		b[12][18] = new Room("Ballroom");
		b[12][19] = new Room("Ballroom");
		b[12][20] = new Room("Ballroom");
		b[12][21] = new Room("Ballroom");
		b[12][22] = new Room("Ballroom");
		b[12][23] = new Room("Ballroom");
		b[12][24] = new Room("Ballroom");
		
		b[13][17] = new Room("Ballroom");
		b[13][18] = new Room("Ballroom");
		b[13][19] = new Room("Ballroom");
		b[13][20] = new Room("Ballroom");
		b[13][21] = new Room("Ballroom");
		b[13][22] = new Room("Ballroom");
		b[13][23] = new Room("Ballroom");
		b[13][24] = new Room("Ballroom");
		
		b[14][17] = new Door(7);
		b[14][18] = new Room("Ballroom");
		b[14][19] = new Room("Ballroom");
		b[14][20] = new Room("Ballroom");
		b[14][21] = new Room("Ballroom");
		b[14][22] = new Room("Ballroom");
		
		b[15][17] = new Room("Ballroom");
		b[15][18] = new Room("Ballroom");
		b[15][19] = new Door(7);
		b[15][20] = new Room("Ballroom");
		b[15][21] = new Room("Ballroom");
		b[15][22] = new Room("Ballroom");
		
		//Center of the board
		b[9][8] = null;
		b[9][9] = null;
		b[9][10] = null;
		b[9][11] = null;
		b[9][12] = null;
		b[9][13] = null;
		b[9][14] = null;
		
		b[10][8] = null;
		b[10][9] = null;
		b[10][10] = null;
		b[10][11] = null;
		b[10][12] = null;
		b[10][13] = null;
		b[10][14] = null;
		
		b[11][8] = null;
		b[11][9] = null;
		b[11][10] = null;
		b[11][11] = null;
		b[11][12] = null;
		b[11][13] = null;
		b[11][14] = null;
		
		b[12][8] = null;
		b[12][9] = null;
		b[12][10] = null;
		b[12][11] = null;
		b[12][12] = null;
		b[12][13] = null;
		b[12][14] = null;
		
		b[13][8] = null;
		b[13][9] = null;
		b[13][10] = null;
		b[13][11] = null;
		b[13][12] = null;
		b[13][13] = null;
		b[13][14] = null;
		
		//Orange part of the board
		b[0][4] = null;
		b[0][6] = null;
		b[0][10] = null;
		b[0][11] = null;
		b[0][17] = null;
		b[0][19] = null;
		b[0][24] = null;
		b[1][24] = null;
		b[2][24] = null;
		b[3][24] = null;
		b[4][24] = null;
		b[5][24] = null;
		b[5][24] = null;
		b[6][24] = null;
		b[7][24] = null;
		b[8][24] = null;
		b[15][24] = null;
		b[16][24] = null;
		b[18][24] = null;
		b[19][24] = null;
		b[20][24] = null;
		b[21][24] = null;
		b[22][24] = null;
		b[23][24] = null;

		b[6][23] = null;
		
		b[8][0] = null;
		
		b[15][0] = null;
		
		b[17][0] = null;
		b[17][23] = null;
		
		b[23][6] = null;
		b[23][8] = null;
		b[23][16] = null;
		b[23][18] = null;
	}
	
	/**
	 * Sets the space on the board at b[@{xCoordinate}][@{yCoordinate}] to @{obj}
	 * @param xCoordinate - x position of object to be returned
	 * @param yCoordinate - y position of object to be returned
	 * @param obj - Object to be set at the position
	 * @return true if able to set false if not
	 */
	public static boolean set(int xCoordinate, int yCoordinate, Object obj){
		b[xCoordinate][yCoordinate] = obj;
		return true;
	}
	
	/**
	 * Returns the object held by the board at b[@{xCoordinate}][@{yCoordinate}]
	 * @param xCoordinate - x position of object to be returned
	 * @param yCoordinate - y position of object to be returned
	 * @return Object held at the space b[xCoordinate][yCoordinate]
	 */
	public static Object get(int xCoordinate, int yCoordinate){
		return b[xCoordinate][yCoordinate];
	}
}
