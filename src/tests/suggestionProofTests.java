package tests;

/**
 * @Author Jared Brown, Sean Rowland
 */

import org.junit.*;
import static org.junit.Assert.*;
import code.*;

import java.util.LinkedList;

public class suggestionProofTests {
	
	//creates players and all cards and dishes out cards to each player
	Board b = new Board();
	Player p1 = new Player("Mrs. Scarlett",2,5, new Hallway());
	Player p2 = new Player("Col. Mustard",2,6, new Hallway());
	Player p3 = new Player("Mrs. White",2,6, new Hallway());
	Player p4 = new Player("Mr. Green",2,6, new Hallway());
	Player p5 = new Player("Mrs. Peacock",2,6, new Hallway());
	Player p6 = new Player("Prof. Plum",2,6, new Hallway());
	String c1 = "Mrs. Scarlett";//leave out
	String c2 = "Col. Mustard";
	String c3 = "Mrs. White";
	String c4 = "Mr. Green";
	String c5 = "Mrs. Peacock";
	String c6 = "Prof. Plum";
	String c7 = "Study";
	String c8 = "Hall";
	String c9 = "Billiard Room";
	String c10 = "Ballroom";
	String c11 = "Conservatory";//leave out
	String c12 = "Kitchen";
	String c13 = "Dining Room";
	String c14 = "Lounge";
	String c15 = "Library";
	String c16 = "Candle Stick";
	String c17 = "Knife";
	String c18 = "Revolver";
	String c19 = "Rope";
	String c20 = "Lead Pipe";
	String c21 = "Wrench";//leave out
	
	boolean beforeRanOnce = false;
	
	@Before
	public void runOnceBeforeTests(){
		if(!beforeRanOnce){ //done so cards only added to players once
			p1.addCard(c2);
			p1.addCard(c8);
			p1.addCard(c15);
			p2.addCard(c3);
			p2.addCard(c9);
			p2.addCard(c16);
			p3.addCard(c4);
			p3.addCard(c10);
			p3.addCard(c17);
			p4.addCard(c5);
			p4.addCard(c12);
			p4.addCard(c18);
			p5.addCard(c6);
			p5.addCard(c13);
			p5.addCard(c19);
			p6.addCard(c7);
			p6.addCard(c14);
			p6.addCard(c20);
			beforeRanOnce = true;
		}
	}
	
	//tests as required by Dr. Hertz on piazza
	
	@Test
	public void hertzSuggestionProofTest1(){ //proved by next player, Player card
		String[] suggestionByPlayer = new String[]{
			c12,
			c4, 
			c18
		};
		Player[] listOfOtherPlayers = new Player[]{
			p3,
			p4,
			p5,
			p6,
			p1,
			p2
		};
		LinkedList<String> sr = p2.suggestionProof(suggestionByPlayer,listOfOtherPlayers);
		assertEquals(p3.getName(), sr.get(0));
		assertEquals(c4, sr.get(1));
	}
	
	@Test
	public void hertzSuggestionProofTest2(){ //proved by next player, Room card
		String[] suggestionByPlayer = new String[]{
			c10,
			c5, 
			c18
		};
		Player[] listOfOtherPlayers = new Player[]{
			p3,
			p4,
			p5,
			p6,
			p1,
			p2
		};
		LinkedList<String> sr = p2.suggestionProof(suggestionByPlayer,listOfOtherPlayers);
		assertEquals(p3.getName(), sr.get(0));
		assertEquals(c10, sr.get(1));
	}
	
	@Test
	public void hertzSuggestionProofTest3(){ //proved by next player, Weapon card
		String[] suggestionByPlayer = new String[]{
			c12,
			c1, 
			c17
		};
		Player[] listOfOtherPlayers = new Player[]{
			p3,
			p4,
			p5,
			p6,
			p1,
			p2
		};
		LinkedList<String> sr = p2.suggestionProof(suggestionByPlayer,listOfOtherPlayers);
		assertEquals(p3.getName(), sr.get(0));
		assertEquals(c17, sr.get(1));
	}
	
	@Test
	public void hertzSuggestionProofTest4(){ //proved by next player, 2 cards
		String[] suggestionByPlayer = new String[]{
			c12,
			c4, 
			c17
		};
		Player[] listOfOtherPlayers = new Player[]{
			p3,
			p4,
			p5,
			p6,
			p1,
			p2
		};
		LinkedList<String> sr = p2.suggestionProof(suggestionByPlayer,listOfOtherPlayers);
		assertEquals(p3.getName(), sr.get(0));
		assertEquals(c4, sr.get(1));
		assertEquals(c17, sr.get(2));
	}
	
	@Test
	public void hertzSuggestionProofTest5(){ //proved by player after next
		String[] suggestionByPlayer = new String[]{
			c5,
			c1, 
			c19
		};
		Player[] listOfOtherPlayers = new Player[]{
			p3,
			p4,
			p5,
			p6,
			p1,
			p2
		};
		LinkedList<String> sr = p2.suggestionProof(suggestionByPlayer,listOfOtherPlayers);
		assertEquals(p4.getName(), sr.get(0));
		assertEquals(c5, sr.get(1));
	}
	
	@Test
	public void hertzSuggestionProofTest6(){ //proved by player immediately before current
		String[] suggestionByPlayer = new String[]{
			c14,
			c7, 
			c20
		};
		Player[] listOfOtherPlayers = new Player[]{
			p2,
			p3,
			p4,
			p5,
			p6,
			p1
		};
		LinkedList<String> sr = p1.suggestionProof(suggestionByPlayer,listOfOtherPlayers);
		assertEquals(p6.getName(), sr.get(0));
		assertEquals(c14, sr.get(1));
		assertEquals(c7, sr.get(2));
	}
	
	@Test
	public void hertzSuggestionProofTest7(){ //no one proves false, current player matches
		String[] suggestionByPlayer = new String[]{
			c11,
			c2, 
			c21
		};
		Player[] listOfOtherPlayers = new Player[]{
			p2,
			p3,
			p4,
			p5,
			p6,
			p1
		};
		LinkedList<String> sr = p1.suggestionProof(suggestionByPlayer,listOfOtherPlayers);
		assertEquals(p1.getName(), sr.get(0));
		assertEquals(c2, sr.get(1));
	}
	
	@Test
	public void hertzSuggestionProofTest8(){ //no one proves suggestion false
		String[] suggestionByPlayer = new String[]{
			c11,
			c1, 
			c21
		};
		Player[] listOfOtherPlayers = new Player[]{
			p2,
			p3,
			p4,
			p5,
			p6,
			p1
		};
		LinkedList<String> sr = p1.suggestionProof(suggestionByPlayer,listOfOtherPlayers);
		assertTrue(sr.isEmpty());
	}
}
