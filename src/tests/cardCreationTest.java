package tests;

import org.junit.*;
import static org.junit.Assert.*;

import java.util.LinkedList;

import code.Card;

public class cardCreationTest {

	String c1 = "Mrs. Scarlett";
	String c2 = "Col. Mustard";
	String c3 = "Mrs. White";
	String c4 = "Rev. Green";
	String c5 = "Mrs. Peacock";
	String c6 = "Prof. Plum";
	
	@Test
	public void cardTest(){
		LinkedList<String> tmp = new LinkedList<String>();
		tmp.add(c1);
		tmp.add(c2);
		tmp.add(c3);
		tmp.add(c4);
		tmp.add(c5);
		tmp.add(c6);
		Card nameCards = new Card(tmp);
		assertTrue(nameCards.contains(c1));
		assertTrue(nameCards.contains(c2));
		assertTrue(nameCards.contains(c3));
		assertTrue(nameCards.contains(c4));
		assertTrue(nameCards.contains(c5));
		assertTrue(nameCards.contains(c6));
	}
}
