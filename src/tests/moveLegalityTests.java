package tests;

/**
 * @author Jared Brown
 */

import org.junit.*;
import static org.junit.Assert.*;
import code.*;

public class moveLegalityTests {

	Board b;
	
	@Before
	public void boardCreation(){
		b = new Board();
	}
	
	//tests as required by Dr. Hertz on piazza
	
	@Test
	public void hertzMoveLegalityTest1(){ //legal move horizontally for number of die rolls
		Player p1 = new Player("Mrs. Scarlett",7,7, new Hallway());
		String userInput = "right right"; //simulates user input for movement
		p1.move(2, userInput);
		assertEquals(Board.get(9, 7), new Player());
		assertEquals(9, p1.getPos()[0]);
		assertEquals(7, p1.getPos()[1]);
	}
	
	@Test
	public void hertzMoveLegalityTest2(){ //legal move vertically for number of die rolls
		Player p1 = new Player("Mrs. Scarlett",7,7, new Hallway());
		String userInput = "up up"; //simulates user input for movement
		p1.move(2, userInput);
		assertEquals(Board.get(7, 5), new Player());
		assertEquals(7, p1.getPos()[0]);
		assertEquals(5, p1.getPos()[1]);
	}
	
	@Test
	public void hertzMoveLegalityTest3(){ //legal move vertically and horizontally for number of die rolls
		Player p1 = new Player("Mrs. Scarlett",7,7, new Hallway());
		String userInput = "up right down down left"; //simulates user input for movement
		p1.move(5, userInput);
		assertEquals(Board.get(7, 8), new Player());
		assertEquals(7, p1.getPos()[0]);
		assertEquals(8, p1.getPos()[1]);
	}
	
	@Test
	public void hertzMoveLegalityTest4(){ //legal move through a door into room
		Player p1 = new Player("Mrs. Scarlett",5,19, new Hallway());
		String userInput = "left"; //simulates user input for movement
		p1.move(5, userInput);
		int[] p1Pos = p1.getPos();
		assertEquals(1,p1Pos[0]);
		assertEquals(21,p1Pos[1]);
		assertEquals(Board.get(1, 21), new Player());
	}
	
	@Test
	public void hertzMoveLegalityTest5(){ //legal move using secret passage
		Player p1 = new Player("Mrs. Scarlett",1,0, new Room());
		String userInput = "left"; //simulates user input for movement
		p1.move(5, userInput);
		int[] p1Pos = p1.getPos();
		assertEquals(20,p1Pos[0]);
		assertEquals(20,p1Pos[1]);
		assertEquals(Board.get(20, 20), new Player());
	}
	
	@Test
	public void hertzMoveLegalityTest6(){ //illegal move more than die roll
		Player p1 = new Player("Mrs. Scarlett",7,7, new Hallway());
		String userInput = "up up up up up up"; //simulates user input for movement
		p1.move(5, userInput);
		assertEquals(Board.get(7, 2), new Player());
		assertEquals(7, p1.getPos()[0]);
		assertEquals(2, p1.getPos()[1]);
		assertFalse(1 == p1.getPos()[1]);
	}
	
	@Test
	public void hertzMoveLegalityTest7(){ //illegal move diagonally
		/*this test case is impossible for us because our players will be deciding their
		moves one at a time based on user input and only valid user inputs will be up, down,
		left or right.
		*/
	}
	
	@Test
	public void hertzMoveLegalityTest8(){ //illegal move that's not contiguous
		/*this test case is impossible for us because our players will be deciding their
		moves one at a time based on user input and only valid user inputs will be up, down,
		left or right. Impossible to move over one space in any one direction so moves
		that are not contiguous cannot happen.
		*/
	}
	
	@Test
	public void hertzMoveLegalityTest9(){ //illegal move through a wall
		Player p1 = new Player("Mrs. Scarlett",7,0, new Hallway());
		assertEquals(0, p1.moveLegality(6, 0));
	}
	
	//teams own tests below
	
	@Test
	public void hertzMoveLegalityTest4pt2(){ //legal move through a door into room, with one other player in room
		Player p1 = new Player("Mrs. Scarlett",5,19, new Hallway());
		Player p2 = new Player("a",1,21, new Room());
		String userInput = "left"; //simulates user input for movement
		p1.move(5, userInput);
		int[] p1Pos = p1.getPos();
		assertEquals(2,p1Pos[0]);
		assertEquals(21,p1Pos[1]);
		assertEquals(Board.get(2, 21), new Player());//change
	}
	
	@Test
	public void hertzMoveLegalityTest4pt3(){ //legal move through a door into room, with all other players in room
		Player p1 = new Player("Mrs. Scarlett",5,19, new Hallway());
		Player p2 = new Player("a",1,21, new Room());
		Player p3 = new Player("a",2,21, new Room());
		Player p4 = new Player("a",3,21, new Room());
		Player p5 = new Player("a",1,22, new Room());
		Player p6 = new Player("a",2,22, new Room());
		String userInput = "left"; //simulates user input for movement
		p1.move(5, userInput);
		int[] p1Pos = p1.getPos();
		assertEquals(3,p1Pos[0]);//change
		assertEquals(22,p1Pos[1]);//change
		assertEquals(Board.get(3, 22), new Player());//change
	}
	
	@Test
	public void hertzMoveLegalityTest5pt2(){ //legal move using secret passage, when one other player in room
		Player p1 = new Player("Mrs. Scarlett",1,0, new Room());
		Player p2 = new Player("b",19,20, new Room());
		String userInput = "left"; //simulates user input for movement
		p1.move(5, userInput);
		int[] p1Pos = p1.getPos();
		assertEquals(20,p1Pos[0]);
		assertEquals(20,p1Pos[1]);
		assertEquals(Board.get(20, 20), new Player());
	}
	
	@Test
	public void hertzMoveLegalityTest5pt3(){ //legal move using secret passage, when all other players in room
		Player p1 = new Player("Mrs. Scarlett",1,0, new Room());
		Player p2 = new Player("a",20,20, new Room());
		Player p3 = new Player("b",21,20, new Room());
		Player p4 = new Player("c",22,20, new Room());
		Player p5 = new Player("d",20,21, new Room());
		Player p6 = new Player("e",21,21, new Room());
		String userInput = "left"; //simulates user input for movement
		p1.move(5, userInput);
		int[] p1Pos = p1.getPos();
		assertEquals(22,p1Pos[0]);
		assertEquals(21,p1Pos[1]);
		assertEquals(Board.get(22, 21), new Player());
	}
	
	@Test
	public void moveLegalityTest1(){ //hallway to hallway, true
		Player p1 = new Player("Mrs. Scarlett",7,0, new Hallway());
		assertEquals(1, p1.moveLegality(7, 1));
	}
	
	@Test
	public void moveLegalityTest2(){ //hallway to room, false
		Player p1 = new Player("Mrs. Scarlett",7,0, new Hallway());
		assertEquals(0, p1.moveLegality(6, 0));
	}
	
	@Test
	public void moveLegalityTest3(){ //hallway to null, false
		Player p1 = new Player("Mrs. Scarlett",7,23, new Hallway());
		assertEquals(0, p1.moveLegality(7, 24));
	}
	
	@Test
	public void moveLegalityTest4(){ //hallway to player, false
		Player p1 = new Player("Mrs. Scarlett",6,6, new Hallway());
		Player p2 = new Player("Mr. Mustard",7,6, new Hallway());
		assertEquals(0, p1.moveLegality(7, 6));
	}
	
	@Test
	public void moveLegalityTest5(){ //hallway to door, true
		Player p1 = new Player("Mrs. Scarlett",6,4, new Hallway());
		assertEquals(4, p1.moveLegality(6, 3));
	}
	
	@Test
	public void moveLegalityTest6(){ //room to secret passage, true
		Player p1 = new Player("Mrs. Scarlett",0,22,new Room());
		assertEquals(3, p1.moveLegality(0, 23));
	}
	
	@Test
	public void moveLegalityTest7(){ //room to room, true
		Player p1 = new Player("Mrs. Scarlett",2,20, new Room());
		assertEquals(2, p1.moveLegality(2, 21));
	}
	
	@Test
	public void moveLegalityTest8(){ //room to door, true
		Player p1 = new Player("Mrs. Scarlett",5,3, new Room());
		assertEquals(2, p1.moveLegality(6, 3));
	}
	
	@Test
	public void moveLegalityTest9(){ //room to player, false
		Player p1 = new Player("Mrs. Scarlett",5,3, new Room());
		Player p2 = new Player("Mr. Mustard",4,3, new Room());
		assertEquals(0, p1.moveLegality(4, 3));
	}
	
	@Test
	public void moveLegalityTest10(){ //room to null, false
		Player p1 = new Player("Mrs. Scarlett",5,23, new Room());
		assertEquals(0, p1.moveLegality(5, 24));
	}
	
	@Test
	public void moveLegalityTest11(){ //room to hallway, false
		Player p1 = new Player("Mrs. Scarlett",5,22, new Room());
		assertEquals(0, p1.moveLegality(6, 22));
	}
	
	@Test
	public void moveLegalityTest12(){ //door to door, true
		Player p1 = new Player("Mrs. Scarlett",11,6, new Door());
		assertEquals(2, p1.moveLegality(12, 6));
	}
	
	@Test
	public void moveLegalityTest13(){ //door to room, true
		Player p1 = new Player("Mrs. Scarlett",11,6, new Door());
		assertEquals(2, p1.moveLegality(11, 5));
	}
	
	@Test
	public void moveLegalityTest14(){ //door to hallway, true
		Player p1 = new Player("Mrs. Scarlett",11,6, new Door());
		assertEquals(1, p1.moveLegality(11, 7));
	}
	
	@Test
	public void moveLegalityTest15(){ //door to player, false
		Player p1 = new Player("Mrs. Scarlett",11,6, new Door());
		Player p2 = new Player("Mr. Mustard",11,7, new Hallway());
		assertEquals(0, p1.moveLegality(11, 7));
	}
}
